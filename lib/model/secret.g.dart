// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'secret.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

KeyValue _$KeyValueFromJson(Map<String, dynamic> json) => KeyValue(
      key: json['key'] as String?,
      value: json['value'] as String?,
    );

Map<String, dynamic> _$KeyValueToJson(KeyValue instance) => <String, dynamic>{
      'key': instance.key,
      'value': instance.value,
    };

SecretData _$SecretDataFromJson(Map<String, dynamic> json) => SecretData(
      type: json['type'] as String?,
      applicationPasswordTitle: json['application_password_title'] as String?,
      applicationPasswordUsername:
          json['application_password_username'] as String?,
      applicationPasswordPassword:
          json['application_password_password'] as String?,
      applicationPasswordNotes: json['application_password_notes'] as String?,
      totpTitle: json['totp_title'] as String?,
      totpCode: json['totp_code'] as String?,
      totpPeriod: json['totp_period'] as String?,
      totpDigits: json['totp_digits'] as String?,
      totpAlgorithm: json['totp_algorithm'] as String?,
      totpUrlFilter: json['totp_url_filter'] as String?,
      websitePasswordTitle: json['website_password_title'] as String?,
      websitePasswordUrl: json['website_password_url'] as String?,
      websitePasswordUsername: json['website_password_username'] as String?,
      websitePasswordPassword: json['website_password_password'] as String?,
      websitePasswordNotes: json['website_password_notes'] as String?,
      websitePasswordAutoSubmit: json['website_password_auto_submit'] as bool?,
      websitePasswordUrlFilter: json['website_password_url_filter'] as String?,
      noteTitle: json['note_title'] as String?,
      noteNotes: json['note_notes'] as String?,
      environmentVariablesTitle: json['environment_variables_title'] as String?,
      environmentVariablesVariables:
          (json['environment_variables_variables'] as List<dynamic>?)
              ?.map((e) => KeyValue.fromJson(e as Map<String, dynamic>))
              .toList(),
      environmentVariablesNotes: json['environment_variables_notes'] as String?,
      fileTitle: json['file_title'] as String?,
      file: json['file'] as String?,
      fileId: json['file_id'] as String?,
      fileShardId: json['file_shard_id'] as String?,
      fileRepositoryId: json['file_repository_id'] as String?,
      fileDestination: json['file_destinations'] as String?,
      fileSecretKey: json['file_secret_key'] as String?,
      fileSize: json['file_size'] as String?,
      fileChunks: json['file_chunks'] as String?,
      creditCardTitle: json['credit_card_title'] as String?,
      creditCardNumber: json['credit_card_number'] as String?,
      creditCardCVC: json['credit_card_cvc'] as String?,
      creditCardPIN: json['credit_card_pin'] as String?,
      creditCardName: json['credit_card_name'] as String?,
      creditCardValidThrough: json['credit_card_valid_through'] as String?,
      creditCardNotes: json['credit_card_notes'] as String?,
      sshOwnKeyTitle: json['ssh_own_key_title'] as String?,
      sshOwnKeyPublic: json['ssh_own_key_public'] as String?,
      sshOwnKeyPrivate: json['ssh_own_key_private'] as String?,
      sshOwnKeyNotes: json['ssh_own_key_notes'] as String?,
      elsterCertificateTitle: json['elster_certificate_title'] as String?,
      elsterCertificateFileContent:
          json['elster_certificate_file_content'] as String?,
      elsterCertificatePassword: json['elster_certificate_password'] as String?,
      elsterCertificateRetrievalCode:
          json['elster_certificate_retrieval_code'] as String?,
      elsterCertificateNotes: json['elster_certificate_notes'] as String?,
      mailGPGOwnKeyTitle: json['mail_gpg_own_key_title'] as String?,
      mailGPGOwnKeyEmail: json['mail_gpg_own_key_email'] as String?,
      mailGPGOwnKeyName: json['mail_gpg_own_key_name'] as String?,
      mailGPGOwnKeyPublic: json['mail_gpg_own_key_public'] as String?,
      mailGPGOwnKeyPrivate: json['mail_gpg_own_key_private'] as String?,
      mailGPGOwnKeyGenerateNew:
          json['mail_gpg_own_key_generate_new'] as String?,
      mailGPGOwnKeyGenerateImportText:
          json['mail_gpg_own_key_generate_import_text'] as String?,
      mailGPGOwnKeyEncryptMessaget:
          json['mail_gpg_own_key_encrypt_message'] as String?,
      mailGPGOwnKeyDecryptMessaget:
          json['mail_gpg_own_key_decrypt_message'] as String?,
      bookmarkTitle: json['bookmark_title'] as String?,
      bookmarkUrl: json['bookmark_url'] as String?,
      bookmarkNotes: json['bookmark_notes'] as String?,
      bookmarkUrlFilter: json['bookmark_url_filter'] as String?,
    );

Map<String, dynamic> _$SecretDataToJson(SecretData instance) =>
    <String, dynamic>{
      'type': instance.type,
      'application_password_title': instance.applicationPasswordTitle,
      'application_password_username': instance.applicationPasswordUsername,
      'application_password_password': instance.applicationPasswordPassword,
      'application_password_notes': instance.applicationPasswordNotes,
      'totp_title': instance.totpTitle,
      'totp_code': instance.totpCode,
      'totp_period': instance.totpPeriod,
      'totp_digits': instance.totpDigits,
      'totp_algorithm': instance.totpAlgorithm,
      'totp_url_filter': instance.totpUrlFilter,
      'website_password_title': instance.websitePasswordTitle,
      'website_password_url': instance.websitePasswordUrl,
      'website_password_username': instance.websitePasswordUsername,
      'website_password_password': instance.websitePasswordPassword,
      'website_password_notes': instance.websitePasswordNotes,
      'website_password_auto_submit': instance.websitePasswordAutoSubmit,
      'website_password_url_filter': instance.websitePasswordUrlFilter,
      'note_title': instance.noteTitle,
      'note_notes': instance.noteNotes,
      'environment_variables_title': instance.environmentVariablesTitle,
      'environment_variables_variables': instance.environmentVariablesVariables,
      'environment_variables_notes': instance.environmentVariablesNotes,
      'file_title': instance.fileTitle,
      'file': instance.file,
      'file_id': instance.fileId,
      'file_shard_id': instance.fileShardId,
      'file_repository_id': instance.fileRepositoryId,
      'file_destinations': instance.fileDestination,
      'file_secret_key': instance.fileSecretKey,
      'file_size': instance.fileSize,
      'file_chunks': instance.fileChunks,
      'credit_card_title': instance.creditCardTitle,
      'credit_card_number': instance.creditCardNumber,
      'credit_card_cvc': instance.creditCardCVC,
      'credit_card_pin': instance.creditCardPIN,
      'credit_card_name': instance.creditCardName,
      'credit_card_valid_through': instance.creditCardValidThrough,
      'credit_card_notes': instance.creditCardNotes,
      'ssh_own_key_title': instance.sshOwnKeyTitle,
      'ssh_own_key_public': instance.sshOwnKeyPublic,
      'ssh_own_key_private': instance.sshOwnKeyPrivate,
      'ssh_own_key_notes': instance.sshOwnKeyNotes,
      'elster_certificate_title': instance.elsterCertificateTitle,
      'elster_certificate_file_content': instance.elsterCertificateFileContent,
      'elster_certificate_password': instance.elsterCertificatePassword,
      'elster_certificate_retrieval_code':
          instance.elsterCertificateRetrievalCode,
      'elster_certificate_notes': instance.elsterCertificateNotes,
      'mail_gpg_own_key_title': instance.mailGPGOwnKeyTitle,
      'mail_gpg_own_key_email': instance.mailGPGOwnKeyEmail,
      'mail_gpg_own_key_name': instance.mailGPGOwnKeyName,
      'mail_gpg_own_key_public': instance.mailGPGOwnKeyPublic,
      'mail_gpg_own_key_private': instance.mailGPGOwnKeyPrivate,
      'mail_gpg_own_key_generate_new': instance.mailGPGOwnKeyGenerateNew,
      'mail_gpg_own_key_generate_import_text':
          instance.mailGPGOwnKeyGenerateImportText,
      'mail_gpg_own_key_encrypt_message': instance.mailGPGOwnKeyEncryptMessaget,
      'mail_gpg_own_key_decrypt_message': instance.mailGPGOwnKeyDecryptMessaget,
      'bookmark_title': instance.bookmarkTitle,
      'bookmark_url': instance.bookmarkUrl,
      'bookmark_notes': instance.bookmarkNotes,
      'bookmark_url_filter': instance.bookmarkUrlFilter,
    };

Secret _$SecretFromJson(Map<String, dynamic> json) => Secret(
      createDate: json['createDate'] as String?,
      writeDate: json['writeDate'] as String?,
      secretId: json['secretId'] as String?,
      type: json['type'] as String?,
      data: json['data'],
      callbackUrl: json['callbackUrl'] as String?,
      callbackUser: json['callbackUser'] as String?,
      callbackPass: json['callbackPass'] as String?,
      secretKey: fromHex(json['secret_key'] as String?),
    );

Map<String, dynamic> _$SecretToJson(Secret instance) => <String, dynamic>{
      'createDate': instance.createDate,
      'writeDate': instance.writeDate,
      'secretId': instance.secretId,
      'type': instance.type,
      'data': instance.data,
      'callbackUrl': instance.callbackUrl,
      'callbackUser': instance.callbackUser,
      'callbackPass': instance.callbackPass,
      'secret_key': toHex(instance.secretKey),
    };
