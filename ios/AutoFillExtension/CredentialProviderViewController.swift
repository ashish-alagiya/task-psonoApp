//
//  CredentialProviderViewController.swift
//  AutoFillExtension
//

import LocalAuthentication
import AuthenticationServices

class CredentialProviderViewController: ASCredentialProviderViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    
    public var unfilteredItems: [Item] = []
    public var items: [Item] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func getLoginAttempts () -> Int {
        let loginAttempts: String? = storage.read(key: "loginAttempts")
        if loginAttempts == nil {
            return 0
        }
        let loginAttemptsInt = Int(loginAttempts!)
        if loginAttemptsInt == nil {
            return 0
        }
        return loginAttemptsInt!
    }
    
    func incrementLoginAttempts () -> Int {
        var loginAttempts = getLoginAttempts()
        loginAttempts += 1
        storage.write(key: "loginAttempts", value: String(loginAttempts))
        return loginAttempts
    }
    
    func resetLoginAttempts () -> Void {
        storage.write(key: "loginAttempts", value: String(0))
    }
    
    func askPassphraseToUnlockPhone(onUnlockSuccess: @escaping () -> Void, onUnlockFailure: @escaping () -> Void) {
        
        let lockscreenPin: String? = storage.read(key: "lockscreenPin")
        
        guard lockscreenPin != nil && lockscreenPin!.count > 0 else {
            onUnlockFailure()
            return
        }
            
        let ac = UIAlertController(title: "Enter passphrase", message: "", preferredStyle: .alert)
        ac.addTextField {
            textfield in
            textfield.placeholder = "Passphrase"
            textfield.isSecureTextEntry = true
        }
        let confirmAction = UIAlertAction(title: "OK", style: .default) {
            [weak ac] _ in
            guard let ac = ac, let textField = ac.textFields?.first else {
                return
            }
            if textField.text == lockscreenPin {
                self.resetLoginAttempts()
                onUnlockSuccess()
            } else {
                let ac = UIAlertController(title: "Passphrase incorrect", message: "The passphrase that you entered did not match the passphrase that you confgiured for Psono.", preferredStyle: .alert)
                func handler(alert: UIAlertAction) {
                    
                    let loginAttempts = self.incrementLoginAttempts()
                    if loginAttempts >= 5 { // we allow the user to try his passphrase 5 times before we lock him out
                        self.resetLoginAttempts()
                        managerDatastoreUser.logout()
                    }
                    onUnlockFailure()
                }
                ac.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
                self.present(ac, animated: true)
            }
        }
        ac.addAction(confirmAction)
        func handler(alert: UIAlertAction) {
            onUnlockFailure()
        }
        ac.addAction(UIAlertAction(title: "Cancel", style: .default, handler: handler))
        self.present(ac, animated: true)
    }
    
    
    
    func askUnlockPhone(onUnlockSuccess: @escaping () -> Void, onUnlockFailure: @escaping () -> Void) {
        let context = LAContext()
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Unlock Psono"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply: {
                (success: Bool, authenticationError: Error?) -> Void in
                DispatchQueue.main.async {
                    if success {
                        self.resetLoginAttempts()
                        onUnlockSuccess()
                    } else {
                        //error. Authentication failed
                        let laError: LAError? = authenticationError as! LAError?
                        let lockscreenPin: String? = storage.read(key: "lockscreenPin")
                        
                        if laError != nil && laError!.code == LAError.userFallback && lockscreenPin != nil && lockscreenPin!.count > 0 {
                            // user requested passphrase and a passphrase is configured so we give him passphrase :)
                            self.askPassphraseToUnlockPhone(onUnlockSuccess: onUnlockSuccess, onUnlockFailure: onUnlockFailure)
                        } else {
                            // biometric authentication failed
                            let ac = UIAlertController(title: "Authentication failed", message: "You could not be authenticated, please try again.", preferredStyle: .alert)
                            func handler(alert: UIAlertAction) {
                                onUnlockFailure()
                            }
                            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
                            self.present(ac, animated: true)
                        }
                    }
                }
            })
        } else {
            // no biometric authentication available
            let lockscreenPin: String? = storage.read(key: "lockscreenPin")
            if lockscreenPin != nil && lockscreenPin!.count > 0 {
                // lets try passphrase
                self.askPassphraseToUnlockPhone(onUnlockSuccess: onUnlockSuccess, onUnlockFailure: onUnlockFailure)
            } else {
                let ac = UIAlertController(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication", preferredStyle: .alert)
                func handler(alert: UIAlertAction) {
                    onUnlockFailure()
                }
                ac.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
                self.present(ac, animated: true)
            }
        }
    }
    
    func loadData() {
        managerDatastorePassword.getPasswordDatastore(datastoreId: nil, completionHandler: {
            (data: Datastore?, error: Error?) -> Void in
            
            self.unfilteredItems = []
            guard data != nil, data!.data != nil else {
                return
            }
            helper.createList(obj: data!.data!, list: &self.unfilteredItems)
            
            self.unfilteredItems = self.unfilteredItems.filter {
                ["application_password", "website_password"].contains($0.type)
            }
            self.filterItems()
            
            DispatchQueue.main.async(execute: {
                self.tableView.reloadData()
            })
        })
    }

    /*
     Prepare your UI to list available credentials for the user to choose from. The items in
     'serviceIdentifiers' describe the service the user is logging in to, so your extension can
     prioritize the most relevant credentials in the list.
    */
    override func prepareCredentialList(for serviceIdentifiers: [ASCredentialServiceIdentifier]) {
        
        var urlString: String? = serviceIdentifiers.first?.identifier
        if urlString == nil {
            urlString = ""
        }
        let url = URL(string: urlString!)
        self.searchBar.text = url?.host

        if self.searchBar.text != nil {
            let splitText = self.searchBar.text?.components(separatedBy: ".")
            if splitText?.count ?? 0 >= 3 {
                self.searchBar.text = splitText![splitText!.count - 2] + "." + splitText![splitText!.count - 1]
            }
        }

        let token: String? = storage.read(key: "token")
        
        // try to fix authentication sometimes failung
        sleep(1)
        
        if token == nil || token!.count < 1 {
            // no biometric authentication available
            let ac = UIAlertController(title: "Login Required", message: "Please login to Psono first and then come back.", preferredStyle: .alert)
            func handler(alert: UIAlertAction) {
                self.extensionContext.cancelRequest(withError: NSError(domain: ASExtensionErrorDomain, code: ASExtensionError.userCanceled.rawValue))
            }
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
            self.present(ac, animated: true)
        } else {
            askUnlockPhone(onUnlockSuccess: {
                [weak self] in
                self?.loadData()
            }, onUnlockFailure: {
                [weak self] in
                self?.extensionContext.cancelRequest(withError: NSError(domain: ASExtensionErrorDomain, code: ASExtensionError.userCanceled.rawValue))
            })
        }
    }
    
    public func filterItems() {
        guard self.searchBar.text != nil && self.searchBar.text != "" else {
            self.items = self.unfilteredItems
            return
        }
        let filter = helper.getPasswordFilter(test: self.searchBar.text!)
        
        self.items = self.unfilteredItems.filter {
            filter($0)
        }
    }

    /*
     Implement this method if your extension supports showing credentials in the QuickType bar.
     When the user selects a credential from your app, this method will be called with the
     ASPasswordCredentialIdentity your app has previously saved to the ASCredentialIdentityStore.
     Provide the password by completing the extension request with the associated ASPasswordCredential.
     If using the credential would require showing custom UI for authenticating the user, cancel
     the request with error code ASExtensionError.userInteractionRequired.

    override func provideCredentialWithoutUserInteraction(for credentialIdentity: ASPasswordCredentialIdentity) {
        let databaseIsUnlocked = true
        if (databaseIsUnlocked) {
            let passwordCredential = ASPasswordCredential(user: "j_appleseed", password: "apple1234")
            self.extensionContext.completeRequest(withSelectedCredential: passwordCredential, completionHandler: nil)
        } else {
            self.extensionContext.cancelRequest(withError: NSError(domain: ASExtensionErrorDomain, code:ASExtensionError.userInteractionRequired.rawValue))
        }
    }
    */

    /*
     Implement this method if provideCredentialWithoutUserInteraction(for:) can fail with
     ASExtensionError.userInteractionRequired. In this case, the system may present your extension's
     UI and call this method. Show appropriate UI for authenticating the user then provide the password
     by completing the extension request with the associated ASPasswordCredential.

    override func prepareInterfaceToProvideCredential(for credentialIdentity: ASPasswordCredentialIdentity) {
    }
    */

    @IBAction func cancel(_ sender: AnyObject?) {
        self.extensionContext.cancelRequest(withError: NSError(domain: ASExtensionErrorDomain, code: ASExtensionError.userCanceled.rawValue))
    }

}

extension CredentialProviderViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.items[indexPath.row]
        managerSecret.readSecret(secretId: item.secretId!, secretKey: item.secretKey!, completionHandler: {
            (secret: Secret?, error: Error?) -> Void in
            
            guard error == nil && secret != nil else {
                // TODO handle error proper and show an error message
                
                let passwordCredential = ASPasswordCredential(user: "", password: "")
                self.extensionContext.completeRequest(withSelectedCredential: passwordCredential, completionHandler: nil)
                return
            }
            
            var user = ""
            var password = ""
            if item.type == "website_password" {
                if secret!.data?.websitePasswordUsername != nil {
                    user = (secret!.data?.websitePasswordUsername)!
                }
                if secret!.data?.websitePasswordPassword != nil {
                    password = (secret!.data?.websitePasswordPassword)!
                }
            } else if item.type == "application_password" {
                if secret!.data?.applicationPasswordUsername != nil {
                    user = (secret!.data?.applicationPasswordUsername)!
                }
                if secret!.data?.applicationPasswordPassword != nil {
                    password = (secret!.data?.applicationPasswordPassword)!
                }
            }
            
            let passwordCredential = ASPasswordCredential(user: user, password:password)
            self.extensionContext.completeRequest(withSelectedCredential: passwordCredential, completionHandler: nil)
            
        })
        
    }
}

extension CredentialProviderViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = self.items[indexPath.row].name
        return cell
    }
}

extension CredentialProviderViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterItems()
        self.tableView.reloadData()
    }
}
