//
//  ReadShare.swift
//  AutoFillExtension
//

import Foundation
import Sodium


class ReadShare: JsonObject {
    public var id: String?
    public var data: Bytes?
    public var dataNonce: Bytes?
    public var userId: String?
    public var rights: ReadShareRight?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.id = jsonDictionary["id"] as? String
        self.data = converter.fromHex(string: jsonDictionary["data"] as? String)
        self.dataNonce = converter.fromHex(string: jsonDictionary["data_nonce"] as? String)
        self.userId = jsonDictionary["user_id"] as? String
        
        
        if jsonDictionary["rights"] != nil {
            if let rightsJsonDictionary = jsonDictionary["rights"] as? [String: Any] {
                self.rights = ReadShareRight.init(jsonDictionary: rightsJsonDictionary)
            }
        }
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.id != nil { jsonDict["id"] = self.id }
        if self.data != nil { jsonDict["data"] = converter.toHex(bytes: self.data) }
        if self.dataNonce != nil { jsonDict["data_nonce"] = converter.toHex(bytes: self.dataNonce) }
        if self.userId != nil { jsonDict["user_id"] = self.userId }
        
        if self.rights != nil {
            jsonDict["rights"] = self.rights!.toJsonDict()
        }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}



class ReadShareRight: JsonObject {
    public var read: Bool?
    public var write: Bool?
    public var grant: Bool?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.read = jsonDictionary["read"] as? Bool
        self.write = jsonDictionary["write"] as? Bool
        self.grant = jsonDictionary["grant"] as? Bool
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.read != nil { jsonDict["read"] = self.read }
        if self.write != nil { jsonDict["write"] = self.write }
        if self.grant != nil { jsonDict["read"] = self.grant }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}
