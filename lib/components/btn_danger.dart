import 'package:flutter/material.dart';
import './btn.dart';

class BtnDanger extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;

  BtnDanger({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Btn(
      text: text,
      onPressed: () {
        onPressed!();
      },
      color: Color(0xFFd9534f),
      textColor: Colors.white,
    );
  }
}
