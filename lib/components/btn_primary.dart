import 'package:flutter/material.dart';
import './btn.dart';
import 'package:psono/theme.dart';

class BtnPrimary extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;
  final BorderRadiusGeometry borderRadius;
  BtnPrimary({
    this.text,
    this.onPressed,
    this.borderRadius = BorderRadius.zero,
  });

  @override
  Widget build(BuildContext context) {
    return Btn(
      borderRadius: borderRadius,
      text: text,
      onPressed: () {
        onPressed!();
      },
      color: primarySwatch.shade500,
      textColor: Colors.white,
    );
  }
}
