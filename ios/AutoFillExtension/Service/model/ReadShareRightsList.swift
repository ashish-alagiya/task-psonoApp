//
//  ReadShareRightsList.swift
//  AutoFillExtension
//

import Foundation

class ReadShareRightsList: JsonObject {
    public var shareRights: [ReadShareRightsListEntry]?
    
    required init() {
        
    }
    
    public required init?(jsonDictionary: [String: Any]){
        
        if jsonDictionary["share_rights"] != nil {
            if let shareRightsJson = jsonDictionary["share_rights"] as? [[String: Any]] {
                self.shareRights = []
                for shareRightJson in shareRightsJson {
                    let shareRight = ReadShareRightsListEntry(jsonDictionary: shareRightJson)
                    if shareRight != nil {
                        self.shareRights?.append(shareRight!)
                    }
                }
            }
        }
        
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.shareRights != nil {
            var shareRights: [[String: Any]] = []
            for shareRight in self.shareRights! {
                shareRights.append(shareRight.toJsonDict())
            }
            jsonDict["share_rights"] = shareRights
        }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
        
    }
}


class ReadShareRightsListEntry: JsonObject {
    public var shareId: String?
    public var read: Bool?
    public var write: Bool?
    public var grant: Bool?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.shareId = jsonDictionary["share_id"] as? String
        self.read = jsonDictionary["read"] as? Bool
        self.write = jsonDictionary["write"] as? Bool
        self.grant = jsonDictionary["grant"] as? Bool
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.shareId != nil { jsonDict["share_id"] = self.shareId }
        if self.read != nil { jsonDict["read"] = self.read }
        if self.write != nil { jsonDict["write"] = self.write }
        if self.grant != nil { jsonDict["grant"] = self.grant }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}

