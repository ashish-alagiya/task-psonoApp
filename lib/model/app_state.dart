import 'package:flutter/foundation.dart';
import 'package:psono/model/config.dart';

class AppState {
  final String? serverUrl;
  final Uint8List? verifyKeyOld;
  final Uint8List? verifyKey;
  final String? username;
  //final String password;
  final String? token;
  final Uint8List? sessionSecretKey;
  final int? lastServerConnectionTimeSinceEpoch;
  final int? lastCacheTimeSinceEpoch;
  final Uint8List? secretKey;
  final Uint8List? publicKey;
  final Uint8List? privateKey;
  final String? lockscreenPin;
  final bool? complianceDisableDeleteAccount;
  final bool? complianceDisableOfflineMode;
  final int? complianceMaxOfflineCacheTimeValid;
  final String? userId;
  final String? userEmail;
  final String? userSauce;
  final Config? config;
  final String? passwordLength;
  final String? lettersUppercase;
  final String? lettersLowercase;
  final String? numbers;
  final String? specialChars;

  AppState({
    required this.serverUrl,
    required this.verifyKeyOld,
    required this.verifyKey,
    required this.username,
    //@required this.password,
    required this.token,
    required this.sessionSecretKey,
    required this.lastServerConnectionTimeSinceEpoch,
    required this.lastCacheTimeSinceEpoch,
    required this.secretKey,
    required this.publicKey,
    required this.privateKey,
    required this.lockscreenPin,
    required this.complianceDisableDeleteAccount,
    required this.complianceDisableOfflineMode,
    required this.complianceMaxOfflineCacheTimeValid,
    required this.userId,
    required this.userEmail,
    required this.userSauce,
    required this.config,
    required this.passwordLength,
    required this.lettersUppercase,
    required this.lettersLowercase,
    required this.numbers,
    required this.specialChars,
  });

  factory AppState.initialState() {
    String serverUrl = '';
    Uint8List verifyKeyOld = Uint8List(0);
    Uint8List verifyKey = Uint8List(0);
    String username = '';
    //String password = '';
    String token = '';
    Uint8List sessionSecretKey = Uint8List(0);
    int lastServerConnectionTimeSinceEpoch = 0;
    int lastCacheTimeSinceEpoch = 0;
    Uint8List secretKey = Uint8List(0);
    Uint8List publicKey = Uint8List(0);
    Uint8List privateKey = Uint8List(0);
    String lockscreenPin = '';
    bool complianceDisableDeleteAccount = false;
    bool complianceDisableOfflineMode = false;
    int complianceMaxOfflineCacheTimeValid = 31536000;
    String userId = '';
    String userEmail = '';
    String userSauce = '';
    Config config = new Config();
    String passwordLength = '';
    String lettersUppercase = '';
    String lettersLowercase = '';
    String numbers = '';
    String specialChars = '';

    return AppState(
      serverUrl: serverUrl,
      verifyKeyOld: verifyKeyOld,
      verifyKey: verifyKey,
      username: username,
      //password: password,
      token: token,
      sessionSecretKey: sessionSecretKey,
      lastServerConnectionTimeSinceEpoch: lastServerConnectionTimeSinceEpoch,
      lastCacheTimeSinceEpoch: lastCacheTimeSinceEpoch,
      secretKey: secretKey,
      publicKey: publicKey,
      privateKey: privateKey,
      lockscreenPin: lockscreenPin,
      complianceDisableDeleteAccount: complianceDisableDeleteAccount,
      complianceDisableOfflineMode: complianceDisableOfflineMode,
      complianceMaxOfflineCacheTimeValid: complianceMaxOfflineCacheTimeValid,
      userId: userId,
      userEmail: userEmail,
      userSauce: userSauce,
      config: config,
      passwordLength: passwordLength,
      lettersUppercase: lettersUppercase,
      lettersLowercase: lettersLowercase,
      numbers: numbers,
      specialChars: specialChars,
    );
  }
}
