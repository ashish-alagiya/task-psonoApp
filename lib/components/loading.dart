import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:psono/theme.dart';

class Loading extends StatelessWidget {
  final String? text;
  final bool hideLoadingIndicator;

  Loading({this.text, this.hideLoadingIndicator = false});

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: SvgPicture.asset(
        'assets/images/logo.svg',
        semanticsLabel: 'Psono Logo',
        height: 70.0,
      ),
    );

    var loadingIndicator;
    var spaceLoadingIndicator;
    if (hideLoadingIndicator) {
      spaceLoadingIndicator = Container();
      loadingIndicator = Container();
    } else {
      spaceLoadingIndicator = const SizedBox(height: 56.0);
      loadingIndicator = Container(
        child: Center(
          child: LinearProgressIndicator(
            backgroundColor: Color(0xFF151f2b),
            valueColor: AlwaysStoppedAnimation<Color>(primarySwatch.shade500),
          ),
        ),
      );
    }

    var loadingMessage;
    var spaceLoadingMessage;
    if (text != null) {
      if (hideLoadingIndicator) {
        spaceLoadingMessage = const SizedBox(height: 56.0);
      } else {
        spaceLoadingMessage = const SizedBox(height: 56.0);
      }
      loadingMessage = Container(
        child: Center(
          child: Text(text!, style: TextStyle(color: primarySwatch.shade500)),
        ),
      );
    } else {
      spaceLoadingMessage = Container();
      loadingMessage = Container();
    }

    return Center(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          logo,
          spaceLoadingIndicator,
          loadingIndicator,
          spaceLoadingMessage,
          loadingMessage,
        ],
      ),
    );
  }
}
