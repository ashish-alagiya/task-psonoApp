import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/storage.dart';
import 'package:url_launcher/url_launcher.dart';

class AutofillOnboardingIOS extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          const SizedBox(height: 24.0),
          Text(
            FlutterI18n.translate(context, "AUTOFILL_SERVICE"),
            textAlign: TextAlign.left,
            style: const TextStyle(color: Color(0xFFb1b6c1)),
          ),
          const SizedBox(height: 16.0),
          component.AlertInfo(
            text: FlutterI18n.translate(
              context,
              "AUTOFILL_SERVICE_ACTIVATE_NOW_INFO_IOS",
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'OPEN_SETTINGS_APP'),
              style: const TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: const Image(
              image: AssetImage("assets/images/iconSettings.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'TAP_INTO_PASSWORDS_AND_ACCOUNTS'),
              style: const TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: const Image(
              image: AssetImage("assets/images/iconPasswordsAccounts.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'TAP_INTO_AUTOFILL_PASSWORDSS'),
              style: const TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: const Image(
              image: AssetImage("assets/images/iconAutofill.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'TURN_ON_AUTOFILL_PASSWORDS'),
              style: const TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: const Image(
              image: AssetImage("assets/images/iconOn.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'SELECT_PSONO'),
              style: const TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: const Image(
              image: AssetImage("assets/images/iconPsonoApp.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'DESELECT_KEYCHAIN'),
              style: const TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: const Image(
              image: AssetImage("assets/images/iconPasswordsAccounts.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          const SizedBox(height: 24.0),
          Row(
            children: <Widget>[
              Expanded(
                child: component.BtnPrimary(
                  onPressed: () async {
                    await storage.write(
                      key: 'passedAutofillOnboarding',
                      value: 'true',
                      iOptions: secureIOSOptions,
                    );
                    if (context.mounted) {
                      Navigator.pushReplacementNamed(context, '/');
                    }
                  },
                  text: FlutterI18n.translate(context, "OK"),
                ),
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          Center(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                    style: const TextStyle(color: Color(0xFF666666)),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () async {
                        final url = Uri.parse(
                            'https://www.psono.pw/privacy-policy.html');
                        if (await canLaunchUrl(url)) {
                          await launchUrl(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
