// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'encrypted_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EncryptedData _$EncryptedDataFromJson(Map<String, dynamic> json) =>
    EncryptedData(
      fromHex(json['text'] as String?),
      fromHex(json['nonce'] as String?),
    );

Map<String, dynamic> _$EncryptedDataToJson(EncryptedData instance) =>
    <String, dynamic>{
      'text': toHex(instance.text),
      'nonce': toHex(instance.nonce),
    };
