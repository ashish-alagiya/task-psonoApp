import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pinput/pinput.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/storage.dart';

class PinConfigurationScreen extends StatefulWidget {
  @override
  _PinConfigurationScreenState createState() => _PinConfigurationScreenState();
}

class _PinConfigurationScreenState extends State<PinConfigurationScreen> {
  bool canContinue = false;
  final controller = TextEditingController();
  final focusNode = FocusNode();

  @override
  void dispose() {
    focusNode.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            Hero(
              tag: 'hero',
              child: SvgPicture.asset(
                'assets/images/logo.svg',
                semanticsLabel: 'Psono Logo',
                height: 70.0,
              ),
            ),
            const SizedBox(height: 48.0),
            Center(
              child: Text(
                FlutterI18n.translate(context, "CONFIGURE_PIN"),
                style: const TextStyle(
                  fontSize: 16,
                  color: Color(0xFF666666),
                ),
              ),
            ),
            const SizedBox(height: 24.0),
            Pinput(
              length: 6,
              hapticFeedbackType: HapticFeedbackType.lightImpact,
              cursor: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 9),
                    width: 22,
                    height: 2,
                    color: Colors.white,
                  ),
                ],
              ),
              controller: controller,
              autofocus: true,
              focusNode: focusNode,
              defaultPinTheme: PinTheme(
                width: 56,
                height: 56,
                textStyle: const TextStyle(
                  fontSize: 36,
                  color: Colors.white,
                ),
                decoration: BoxDecoration(
                  color: const Color(0xFF182535),
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Colors.transparent),
                ),
              ),
              onChanged: (code) {
                bool newCanContinue = code.length == 6;
                if (canContinue != newCanContinue) {
                  setState(() {
                    canContinue = newCanContinue;
                  });
                }
              },
            ),
            const SizedBox(height: 48.0),
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: component.BtnPrimary(
                      text: FlutterI18n.translate(context, "CONTINUE"),
                      onPressed: canContinue
                          ? () async {
                              await storage.write(
                                key: 'lockscreenPin',
                                value: controller.text,
                                iOptions: secureIOSOptions,
                              );
                              if (context.mounted) {
                                Navigator.pushReplacementNamed(context, '/');
                              }
                            }
                          : null,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
