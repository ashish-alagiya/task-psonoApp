import 'package:flutter/material.dart';
import './btn.dart';

class BtnWarning extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;

  BtnWarning({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Btn(
      text: text,
      onPressed: () {
        onPressed!();
      },
      color: Color(0xFFf0ad4e),
      textColor: Colors.white,
    );
  }
}
