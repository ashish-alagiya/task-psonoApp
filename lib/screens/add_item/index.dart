import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/model/otp.dart';
import 'package:psono/model/parsed_url.dart';
import 'package:psono/model/secret.dart';
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/model/share_right.dart';
import 'package:psono/screens/custom_drawer.dart';
import 'package:psono/screens/scan_qr/index.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/crypto_library.dart' as cryptoLibary;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/item_blueprint.dart' as itemBlueprint;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_datastore_setting.dart'
    as managerDatastoreSetting;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;
import 'package:psono/services/manager_secret.dart' as managerSecret;
import 'package:psono/services/manager_share.dart' as managerShare;

class AddItemScreen extends StatefulWidget {
  static String tag = 'add-item-screen';
  final datastoreModel.Folder? parent;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<String?>? path;
  final List<String?>? relativePath;

  AddItemScreen(
      {this.parent, this.datastore, this.share, this.path, this.relativePath});

  @override
  _AddItemScreenState createState() => _AddItemScreenState();
}

class _AddItemScreenState extends State<AddItemScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String? type;
  final Secret _secret = Secret(
    data: {},
  );
  bool _obscurePassword = true;
  OTP? otp;
  datastoreModel.Datastore? settingsDatastore;
  List<TextEditingController> controllers = [];

  void _showErrorDiaglog(String title, String? content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(FlutterI18n.translate(context, title)),
          content: Text(FlutterI18n.translate(context, content!)),
          actions: <Widget>[
            TextButton(
              child: Text(FlutterI18n.translate(context, "CLOSE")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> loadDatastore() async {
    component.Loader.show(context);
    datastoreModel.Datastore? newDatastore;
    try {
      newDatastore = await managerDatastoreSetting.getSettingsDatastore();
    } on apiClient.ServiceUnavailableException {
      if (context.mounted) {
        _showErrorDiaglog(
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    } on apiClient.UnauthorizedException {
      managerDatastoreUser.logout();

      if (context.mounted) {
        Navigator.pushReplacementNamed(context, '/signin/');
      }
      return;
    } finally {
      component.Loader.hide();
    }
    setState(() {
      settingsDatastore = newDatastore;
    });
  }

  Future<void> initStateAsync() async {
    await loadDatastore();
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      initStateAsync();
    });
  }

  @override
  void dispose() {
    component.Loader.hide();
    for (var i = 0; i < controllers.length; i++) {
      controllers[i].dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> menuList = [];

    if (settingsDatastore != null) {
      List blueprints = itemBlueprint.getBlueprints();
      blueprints.sort((a, b) => FlutterI18n.translate(context, a.name)
          .compareTo(FlutterI18n.translate(context, b.name)));

      menuList = [];
      blueprints.forEach((blueprint) {
        var visible = blueprint.settingFieldDefault;
        for (var i = 0; i < settingsDatastore!.dataKV.length; i++) {
          if (settingsDatastore!.dataKV[i]['key'] != blueprint.settingField ||
              settingsDatastore!.dataKV[i]['value'] == null) {
            continue;
          }
          visible = settingsDatastore!.dataKV[i]['value'];
        }

        if (!visible) {
          return;
        }
        menuList.add(
          Card(
            child: ListTile(
              title: Text(FlutterI18n.translate(context, blueprint.name)),
              leading: Icon(blueprint.icon),
              trailing: const Icon(Icons.keyboard_arrow_right),
              onTap: () async {
                if (blueprint.id == 'totp') {
                  final String? result = await Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ScanQR()),
                  );
                  if (result != null) {
                    otp = helper.parseTOTPUri(result);
                  }
                }
                setState(() {
                  type = blueprint.id;
                });
              },
            ),
          ),
        );
      });
    }

    if (type != null) {
      var _bp = itemBlueprint.getBlueprint(type)!;

      int getFieldLength() {
        int advancedCount =
            _bp.fields!.where((field) => field.position == 'advanced').length;
        return _bp.fields!.length - advancedCount + 1;
      }

      return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(
                context,
                'NEW',
              ) +
              ' ' +
              FlutterI18n.translate(
                context,
                _bp.name,
              )),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        backgroundColor: Color(0xFFebeeef),
        body: Card(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            child: Form(
              key: _formKey,
              child: ListView.builder(
                // Let the ListView know how many items it needs to build.
                itemCount: getFieldLength(),
                // Provide a builder function. This is where the magic happens.
                // Convert each item into a widget based on the type of item it is.
                itemBuilder: (c, index) {
                  if (index == getFieldLength() - 1) {
                    // last element, render the save button

                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: component.BtnSuccess(
                        onPressed: () async {
                          if (!_formKey.currentState!.validate()) {
                            return;
                          }
                          component.Loader.show(context);
                          if (this.widget.parent == null ||
                              this.widget.parent!.items == null) {
                            this.widget.parent!.items = [];
                          }

                          _secret.type = this.type;

                          String linkId = await cryptoLibary.generateUUID();

                          String? parentDatastoreId;
                          String? parentShareId;
                          if (this.widget.share == null) {
                            parentDatastoreId =
                                this.widget.datastore!.datastoreId;
                          } else {
                            parentShareId = this.widget.share!.shareId;
                          }
                          String callbackUrl = '';
                          String callbackUser = '';
                          String callbackPass = '';

                          for (final field in _bp.fields!
                              .where((field) => field.inputFormatter != null)) {
                            if (_secret.data.containsKey(field.name)) {
                              field.inputFormatter!.formatEditUpdate(
                                TextEditingValue.empty,
                                TextEditingValue(
                                  text: _secret.data[field.name],
                                ),
                              );
                              _secret.data[field.name] =
                                  field.inputFormatter!.getUnmaskedText();
                            }
                          }

                          Secret createdSecret =
                              await managerSecret.createSecret(
                            _secret.data,
                            linkId,
                            parentDatastoreId,
                            parentShareId,
                            callbackUrl,
                            callbackUser,
                            callbackPass,
                          );
                          _secret.secretId = createdSecret.secretId;
                          _secret.secretKey = createdSecret.secretKey;

                          datastoreModel.Item newItem = datastoreModel.Item(
                            id: linkId,
                            name: _secret.data[_bp.titleField],
                            type: this.type,
                            secretId: createdSecret.secretId,
                            secretKey: createdSecret.secretKey,
                          );

                          if (_bp.urlfilterField != null) {
                            newItem.urlfilter =
                                _secret.data[_bp.urlfilterField];
                          }

                          if (this.widget.share == null) {
                            newItem.shareRights = ShareRight(
                              read: true,
                              write: true,
                              grant: true,
                              delete: true,
                            );
                          } else {
                            newItem.shareRights = ShareRight(
                              read: this.widget.share!.shareRights!.read,
                              write: this.widget.share!.shareRights!.write,
                              grant: this.widget.share!.shareRights!.grant! &&
                                  this.widget.share!.shareRights!.write!,
                              delete: this.widget.share!.shareRights!.write,
                            );
                          }

                          this.widget.parent!.items!.add(newItem);

                          if (this.widget.share == null) {
                            datastoreModel.Datastore datastore =
                                await managerDatastorePassword
                                    .getPasswordDatastore(
                              this.widget.datastore!.datastoreId,
                            );

                            datastoreModel.Folder? parent;
                            if (this.widget.path!.length == 0) {
                              parent = datastore.data;
                            } else {
                              List<String> pathCopy =
                                  new List<String>.from(this.widget.path!);
                              List search =
                                  managerDatastorePassword.findInDatastore(
                                pathCopy,
                                datastore.data,
                              );
                              parent = search[0][search[1]];
                            }

                            if (parent!.items == null) {
                              parent.items = [];
                            }
                            parent.items!.add(newItem);
                            await datastore.save();
                          } else {
                            shareModel.Share share =
                                await managerShare.readShare(
                              this.widget.share!.shareId,
                              this.widget.share!.shareSecretKey,
                            );
                            datastoreModel.Folder? parent;
                            if (this.widget.relativePath!.length == 0) {
                              parent = share.folder;
                            } else {
                              List<String> pathCopy = List<String>.from(
                                this.widget.relativePath!,
                              );
                              List search =
                                  managerDatastorePassword.findInDatastore(
                                pathCopy,
                                share.folder,
                              );
                              parent = search[0][search[1]];
                            }
                            if (parent!.items == null) {
                              parent.items = [];
                            }
                            parent.items!.add(newItem);
                            await share.save();
                          }
                          component.Loader.hide();
                          if (mounted) {
                            Navigator.pop(context);
                          }
                          return;
                        },
                        text: FlutterI18n.translate(context, "SAVE"),
                      ),
                    );
                  } else {
                    // render field
                    final field = _bp.fields![index];

                    String? content = '';
                    List contentKV = [];
                    if (field.field == 'key_value_list') {
                      _secret.data[field.name] = contentKV;
                    } else {
                      if (_secret.data.containsKey(field.name)) {
                        content = _secret.data[field.name].toString();
                      }
                    }
                    if (_bp.id == 'totp' && this.otp != null && content == '') {
                      if (field.name == 'totp_title') {
                        content = this.otp!.getDescription();
                        _secret.data[field.name] = content;
                      }
                      if (field.name == 'totp_code') {
                        content = this.otp!.secret;
                        _secret.data[field.name] = content;
                      }
                      if (field.name == 'totp_period') {
                        content = this.otp!.period.toString();
                        _secret.data[field.name] = this.otp!.period;
                      }
                      if (field.name == 'totp_algorithm') {
                        content = this.otp!.algorithm;
                        _secret.data[field.name] = content;
                      }
                      if (field.name == 'totp_digits') {
                        content = this.otp!.digits.toString();
                        _secret.data[field.name] = this.otp!.digits;
                      }
                    }

                    TextInputType? keyboardType;
                    bool obscureText = false;
                    int? maxLines = 1;
                    List<DropdownMenuItem<String>> buttons = [];
                    List<MaskTextInputFormatter> inputFormatters = [];
                    if (field.inputFormatter != null) {
                      inputFormatters.add(field.inputFormatter!);
                    }

                    if (field.field == 'input' && field.type == 'text') {
                    } else if (field.field == 'input' &&
                        field.type == 'password') {
                      obscureText = true && _obscurePassword;
                      buttons.add(
                        DropdownMenuItem<String>(
                          value: "show-hide",
                          child: Row(children: <Widget>[
                            Icon(
                              _obscurePassword
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Color(0xFF2dbb93),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12.0,
                              ),
                              child: field.name == 'credit_card_pin'
                                  ? Text(
                                      _obscurePassword
                                          ? FlutterI18n.translate(
                                              context,
                                              "SHOW_PIN",
                                            )
                                          : FlutterI18n.translate(
                                              context,
                                              "HIDE_PIN",
                                            ),
                                    )
                                  : Text(
                                      _obscurePassword
                                          ? FlutterI18n.translate(
                                              context,
                                              "SHOW_PASSWORD",
                                            )
                                          : FlutterI18n.translate(
                                              context,
                                              "HIDE_PASSWORD",
                                            ),
                                    ),
                            ),
                          ]),
                        ),
                      );
                      buttons.add(
                        DropdownMenuItem<String>(
                          value: "generate",
                          child: Row(children: <Widget>[
                            const Icon(
                              component.FontAwesome.cogs,
                              color: Color(0xFF2dbb93),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12.0),
                              child: Text(
                                FlutterI18n.translate(
                                    context, "GENERATE_PASSWORD"),
                              ),
                            ),
                          ]),
                        ),
                      );
                    } else if (field.field == 'input' &&
                        field.type == 'totp_code') {
                      obscureText = true && _obscurePassword;
                      buttons.add(
                        DropdownMenuItem<String>(
                          value: "show-hide",
                          child: Row(children: <Widget>[
                            Icon(
                              _obscurePassword
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Color(0xFF2dbb93),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12.0),
                              child: Text(
                                _obscurePassword
                                    ? FlutterI18n.translate(context, "SHOW")
                                    : FlutterI18n.translate(context, "HIDE"),
                              ),
                            ),
                          ]),
                        ),
                      );
                      buttons.add(
                        DropdownMenuItem<String>(
                          value: "scan-totp-code",
                          child: Row(children: <Widget>[
                            const Icon(
                              component.FontAwesome.qrcode,
                              color: Color(0xFF2dbb93),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12.0,
                              ),
                              child: Text(
                                FlutterI18n.translate(context, "SCAN_QR_CODE"),
                              ),
                            ),
                          ]),
                        ),
                      );
                    } else if (field.field == 'input' &&
                        field.type == 'checkbox') {
                    } else if (field.field == 'textarea') {
                      keyboardType = TextInputType.multiline;
                      maxLines = 10;
                    } else if (field.field == 'button' &&
                        field.type == 'button') {
                      keyboardType = TextInputType.multiline;
                    } else if (field.field == 'key_value_list') {
                    } else {
                      throw ("unknown field type combi");
                    }

                    final textController = TextEditingController(
                      text: content,
                    );

                    controllers.add(textController);
                    List<Widget> children = [];
                    if (field.field == 'key_value_list') {
                      children.add(
                        component.KeyValueList(
                          contentKV: contentKV,
                        ),
                      );
                    } else {
                      children.add(
                        Stack(
                          alignment: Alignment.centerRight,
                          children: [
                            TextFormField(
                              obscureText: obscureText,
                              controller: textController,
                              inputFormatters: inputFormatters,
                              validator: (value) {
                                if (field.required &&
                                    (value == null || value.isEmpty)) {
                                  return FlutterI18n.translate(
                                      context, field.errorMessageRequired!);
                                }
                                if (field.field == 'input' &&
                                    field.type == 'totp_code' &&
                                    !helper.isValidTOTPCode(value)) {
                                  return FlutterI18n.translate(
                                      context, 'INVALID_SECRET');
                                }
                                return null;
                              },
                              keyboardType: keyboardType,
                              maxLines: maxLines,
                              onChanged: (text) {
                                _secret.data[field.name] = text;
                                if (field.field == 'input' &&
                                    ['website_password_url', 'bookmark_url']
                                        .contains(field.name) &&
                                    _bp.urlfilterField != null) {
                                  if (text == '') {
                                    _secret.data[_bp.urlfilterField] = '';
                                  } else {
                                    ParsedUrl parsedUrl = helper.parseUrl(text);
                                    _secret.data[_bp.urlfilterField] =
                                        parsedUrl.authority;
                                  }
                                }
                              },
                              decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.fromLTRB(
                                      0, 10, 48.0, 10),
                                  labelText: FlutterI18n.translate(
                                      context, field.title!)),
                            ),
                            DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                icon: Icon(component.FontAwesome.ellipsis_v),
                                iconSize: 20,
                                dropdownColor: Colors.white,
                                onChanged: (String? newValue) async {
                                  if (newValue == 'show-hide') {
                                    setState(() {
                                      _obscurePassword = !_obscurePassword;
                                    });
                                  }
                                  if (newValue == 'generate') {
                                    textController.text =
                                        await managerDatastorePassword
                                            .generate();
                                    _secret.data[field.name] =
                                        textController.text;
                                  }
                                  if (newValue == 'scan-totp-code') {
                                    if (mounted) {
                                      final String? result =
                                          await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => ScanQR()),
                                      );
                                      if (result != null) {
                                        var otp = helper.parseTOTPUri(result);
                                        _secret.data[field.name] = otp.secret;
                                        textController.text = otp.secret!;
                                      }
                                    }
                                  }
                                },
                                items: buttons,
                              ),
                            )
                          ],
                        ),
                      );
                    }

                    return Container(
                      child: Column(
                        children: children,
                      ),
                    );
                  }
                },
              ),
            ),
          ),
        ),
        drawer: CustomDrawer(),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(context, 'NEW_ENTRY')),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        backgroundColor: Color(0xFFebeeef),
        body: Container(
            padding: const EdgeInsets.all(5.0),
            child: ListView(children: menuList)),
        drawer: CustomDrawer(),
      );
    }
  }
}
