import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/autofill.dart' as autofillService;
import 'package:psono/services/storage.dart';
import 'package:url_launcher/url_launcher.dart';

class AutofillOnboardingAndroid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: SvgPicture.asset(
        'assets/images/logo.svg',
        semanticsLabel: 'Psono Logo',
        height: 70.0,
      ),
    );

    return Center(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          logo,
          const SizedBox(height: 24.0),
          Text(
            FlutterI18n.translate(context, "AUTOFILL_SERVICE"),
            textAlign: TextAlign.left,
            style: const TextStyle(color: Color(0xFFb1b6c1)),
          ),
          const SizedBox(height: 16.0),
          component.AlertInfo(
            text: FlutterI18n.translate(
              context,
              "AUTOFILL_SERVICE_ACTIVATE_NOW_INFO",
            ),
          ),
          const SizedBox(height: 24.0),
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 5.0),
                  child: component.BtnPrimary(
                    onPressed: () async {
                      await storage.write(
                        key: 'passedAutofillOnboarding',
                        value: 'true',
                        iOptions: secureIOSOptions,
                      );
                      await autofillService.enable();
                      if (context.mounted) {
                        Navigator.pushReplacementNamed(context, '/');
                      }
                    },
                    text: FlutterI18n.translate(context, "ACTIVATE"),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      foregroundColor: Color(0xFFb1b6c1),
                    ),
                    onPressed: () async {
                      await storage.write(
                        key: 'passedAutofillOnboarding',
                        value: 'true',
                        iOptions: secureIOSOptions,
                      );
                      if (context.mounted) {
                        Navigator.pushReplacementNamed(context, '/');
                      }
                    },
                    child: Text(FlutterI18n.translate(context, "SKIP")),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          Center(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                    style: const TextStyle(color: Color(0xFF666666)),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () async {
                        final url = Uri.parse(
                            'https://www.psono.pw/privacy-policy.html');
                        if (await canLaunchUrl(url)) {
                          await launchUrl(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
