import 'package:json_annotation/json_annotation.dart';

part 'delete_share_link.g.dart';

@JsonSerializable()
class DeleteShareLink {
  DeleteShareLink();

  factory DeleteShareLink.fromJson(Map<String, dynamic> json) =>
      _$DeleteShareLinkFromJson(json);

  Map<String, dynamic> toJson() => _$DeleteShareLinkToJson(this);
}
