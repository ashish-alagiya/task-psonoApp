import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:app_group_directory/app_group_directory.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:psono/model/encrypted_data.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/manager_datastore.dart' as managerDatastore;
import 'package:psono/services/storage.dart';
import 'package:sqlite3/sqlite3.dart';

Database? database;
Uint8List? storageKey;
Map<String, apiClient.ReadMetadataDatastore> metadataDatastores = {};
Map<String, apiClient.ReadMetadataShare> metadataShares = {};
Map<String, apiClient.ReadMetadataSecret> metadataSecrets = {};

/// Main function to update the cache. Will read all datastores and go through
/// all of them with their nested shares and sub shares to update the cache.
/// If  [ratelimit] is set, the cache will only update at max once per hour.
Future<bool> incrementalUpdate({ratelimit: true}) async {
  bool isLoggedIn =
      reduxStore.state.token != null && reduxStore.state.token != "";
  if (!isLoggedIn) {
    return false;
  }
  if (ratelimit &&
      reduxStore.state.lastCacheTimeSinceEpoch >
          DateTime.now().millisecondsSinceEpoch - 60 * 60 * 1000) {
    // we only resync the cache every hour if the ratelimit parameter is true
    return false;
  }

  int nowInMillisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch;
  reduxStore.dispatch(
    SetLastCacheTimeSinceEpochReducerAction(
      nowInMillisecondsSinceEpoch,
    ),
  );
  await storage.write(
    key: 'lastCacheTimeSinceEpoch',
    value: nowInMillisecondsSinceEpoch.toString(),
    iOptions: secureIOSOptions,
  );

  apiClient.readShareRightsOverview(
      reduxStore.state.token, reduxStore.state.sessionSecretKey);

  apiClient.ReadDatastoreList? readDatastoreList =
      await managerDatastore.getDatastoreOverview();

  metadataDatastores = {};
  metadataShares = {};
  metadataSecrets = {};

  bool success = true;
  for (var datastore in readDatastoreList!.datastores!) {
    success = await incrementalUpdateByDatastore(datastore.id!);
  }
  return success;
}

/// Checks whether a cached datastore needs to be updated and recached.
bool datastoreCacheNeedUpdate(datastoreId) {
  if (database == null) {
    return true;
  }
  final ResultSet resultSet = database!.select("""SELECT write_date_milliseconds
      FROM offline_cache
      WHERE connection_type = ? AND endpoint = ?""", [
    'GET'.toLowerCase(),
    '/datastore/$datastoreId/'.toLowerCase(),
  ]);

  bool needUpdate = false;
  if (resultSet.isEmpty) {
    needUpdate = true;
  } else if (resultSet.first['write_date_milliseconds'] <
      helper.isoToMillisecondsSinceEpoch(
        metadataDatastores[datastoreId]!.writeDate!,
      )) {
    needUpdate = true;
  }

  return needUpdate;
}

/// Checks whether a cached share needs to be updated and recached.
bool shareCacheNeedUpdate(shareId) {
  if (database == null) {
    return true;
  }
  final ResultSet resultSet = database!.select("""SELECT write_date_milliseconds
      FROM offline_cache
      WHERE connection_type = ? AND endpoint = ?""", [
    'GET'.toLowerCase(),
    '/share/$shareId/'.toLowerCase(),
  ]);

  bool needUpdate = false;
  if (resultSet.isEmpty) {
    needUpdate = true;
  } else if (resultSet.first['write_date_milliseconds'] <
      helper.isoToMillisecondsSinceEpoch(
        metadataShares[shareId]!.writeDate!,
      )) {
    needUpdate = true;
  }

  return needUpdate;
}

/// Checks whether a cached secret needs to be updated and recached.
bool secretCacheNeedUpdate(secretId) {
  if (database == null) {
    return true;
  }
  final ResultSet resultSet = database!.select("""SELECT write_date_milliseconds
      FROM offline_cache
      WHERE connection_type = ? AND endpoint = ?""", [
    'GET'.toLowerCase(),
    '/secret/$secretId/'.toLowerCase(),
  ]);

  bool needUpdate = false;
  if (resultSet.isEmpty) {
    needUpdate = true;
  } else if (resultSet.first['write_date_milliseconds'] <
      helper.isoToMillisecondsSinceEpoch(
        metadataSecrets[secretId]!.writeDate!,
      )) {
    needUpdate = true;
  }

  return needUpdate;
}

/// An incremental update of the cache of a share and the content for a
/// specific share
Future<void> incrementalUpdateByShares(String shareId) async {
  if (metadataShares.containsKey(shareId)) {
    return;
  }

  apiClient.ReadMetadataShare metadataShare;

  try {
    metadataShare = await apiClient.readMetadataShare(
      reduxStore.state.token,
      reduxStore.state.sessionSecretKey,
      shareId,
    );
  } on apiClient.BadRequestException catch (e) {
    return;
  }

  metadataShares[shareId] = metadataShare;

  if (metadataShare.shares != null) {
    for (var share in metadataShare.shares!) {
      await incrementalUpdateByShares(share.id!);
    }
  }

  if (metadataShare.secrets != null) {
    for (var secret in metadataShare.secrets!) {
      metadataSecrets[secret.id!] = secret;
    }
  }
}

/// An incremental update of the cache of a datastore and the content for a
/// specific datastore
Future<bool> incrementalUpdateByDatastore(String datastoreId) async {
  apiClient.ReadMetadataDatastore metadataDatastore =
      apiClient.ReadMetadataDatastore();
  try {
    metadataDatastore = await apiClient.readMetadataDatastore(
      reduxStore.state.token,
      reduxStore.state.sessionSecretKey,
      datastoreId,
    );
  } catch (e) {
    return false;
  }

  metadataDatastores[datastoreId] = metadataDatastore;

  if (metadataDatastore.shares != null) {
    for (var share in metadataDatastore.shares!) {
      await incrementalUpdateByShares(share.id!);
    }
  }
  if (metadataDatastore.secrets != null) {
    for (var secret in metadataDatastore.secrets!) {
      metadataSecrets[secret.id!] = secret;
    }
  }

  if (datastoreCacheNeedUpdate(datastoreId)) {
    try {
      await apiClient.readDatastore(
        reduxStore.state.token,
        reduxStore.state.sessionSecretKey,
        datastoreId,
      );
    } catch (e) {
      return false;
    }
  }

  for (var shareId in metadataShares.keys) {
    if (shareCacheNeedUpdate(shareId)) {
      try {
        await apiClient.readShare(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
          shareId,
        );
      } catch (e) {
        //pass
      }
    }
  }
  for (var secretId in metadataSecrets.keys) {
    if (secretCacheNeedUpdate(secretId)) {
      try {
        await apiClient.readSecret(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
          secretId,
        );
      } catch (e) {
        //pass
      }
    }
  }
  return true;
}

/// Returns the database directory.
///
/// - `PathUtils.getDataDirectory` API on Android
/// - containerURLForSecurityApplicationGroupIdentifier group.com.psono.app on iOS
Future<Directory?> databaseDirectory() async {
  Directory docDir = await getApplicationDocumentsDirectory();

  if (Platform.isIOS) {
    Directory? appGroupDirectory =
        await AppGroupDirectory.getAppGroupDirectory('group.com.psono.app');
    if (appGroupDirectory == null) {
      return null;
    }
    docDir = appGroupDirectory;
  }

  if (!await docDir.exists()) {
    await docDir.create(recursive: true);
  }
  return docDir;
}

/// Deletes the complete cache. The cache DB neets to be re-initialized with init()
/// again later before it can be used.
Future<void> delete() async {
  if (database == null) {
    return;
  }
  final docDir = await databaseDirectory();
  if (docDir == null) {
    return;
  }
  database!.dispose();
  String dbPath = p.join(docDir.path, "psono_oc.db");
  var file = File(dbPath);
  if (await file.exists()) {
    file.delete();
  }
  database = null;
}

/// Deletes the complete cache. The cache DB neets to be re-initialized with init()
/// again later before it can be used.
Future<void> wipe() async {
  if (database != null) {
    database!.execute("""DROP TABLE IF EXISTS offline_cache""");
  }
  await init();
}

Future<void> init() async {
  String? storageKeyHex = await storage.read(key: 'storageKey');
  if (storageKeyHex != null) {
    storageKey = converter.fromHex(storageKeyHex);
  }
  if (storageKey == null) {
    storageKey = await cryptoLibrary.generateSecretKey();

    await storage.write(
      key: 'storageKey',
      value: converter.toHex(storageKey),
      iOptions: secureIOSOptions,
    );
  }

  final docDir = await databaseDirectory();
  if (docDir == null) {
    return;
  }

  String dbPath = p.join(docDir.path, "psono_oc.db");

  database = sqlite3.open(dbPath);
  database!.execute("""CREATE TABLE IF NOT EXISTS offline_cache (
    connection_type TEXT NOT NULL,
    endpoint TEXT NOT NULL,
    write_date_milliseconds int NOT NULL,
    response TEXT,
    response_nonce TEXT,
    PRIMARY KEY (connection_type, endpoint)
  )""");
}

/// Caches the response for a specific connection type [connectionType] and [endpoint]
Future<void> set(
  String connectionType,
  String endpoint,
  http.Response response,
) async {
  if (reduxStore.state.complianceDisableOfflineMode) {
    return;
  }

  int writeDateSeconds = 0;
  try {
    Map bodyJsonDecoded = jsonDecode(response.body);
    if (bodyJsonDecoded.containsKey('write_date')) {
      writeDateSeconds =
          helper.isoToMillisecondsSinceEpoch(bodyJsonDecoded['write_date']);
    }
  } catch (e) {
    // pass
  }

  final jsonEncodedContent = jsonEncode({
    'statusCode': response.statusCode,
    'body': response.body,
    'headers': response.headers,
    'connectionType': connectionType.toLowerCase(),
    'endpoint': endpoint.toLowerCase(),
  });

  final EncryptedData encryptedResponse = await cryptoLibrary.encryptData(
    jsonEncodedContent,
    storageKey!,
  );

  if (database == null) {
    return;
  }
  database!.execute(
    """REPLACE INTO offline_cache (connection_type, endpoint, write_date_milliseconds, response, response_nonce)
  VALUES(?, ?, ?, ?, ?)""",
    [
      connectionType.toLowerCase(),
      endpoint.toLowerCase(),
      writeDateSeconds,
      converter.toHex(encryptedResponse.text),
      converter.toHex(encryptedResponse.nonce),
    ],
  );
}

/// Reads the previously cached response for a specific [connectionType] and [endpoint]
Future<http.Response?> get(
  String connectionType,
  String endpoint,
) async {
  if (database == null) {
    return null;
  }

  final ResultSet resultSet =
      database!.select("""SELECT response, response_nonce
      FROM offline_cache
      WHERE connection_type = ? AND endpoint = ?""", [
    connectionType.toLowerCase(),
    endpoint.toLowerCase(),
  ]);

  if (resultSet.isEmpty) {
    return null;
  }

  if (reduxStore.state.lastServerConnectionTimeSinceEpoch +
          reduxStore.state.complianceMaxOfflineCacheTimeValid <
      DateTime.now().millisecondsSinceEpoch) {
    return null;
  }

  final String decryptedResponseJson = await cryptoLibrary.decryptData(
    converter.fromHex(resultSet.first['response'])!,
    converter.fromHex(resultSet.first['response_nonce'])!,
    storageKey!,
  );
  Map decryptedResponse = jsonDecode(decryptedResponseJson);

  if (decryptedResponse['connectionType'] != connectionType.toLowerCase() ||
      decryptedResponse['endpoint'] != endpoint.toLowerCase()) {
    return null;
  }

  Map<String, String> headers = Map<String, String>();
  decryptedResponse['headers'].forEach((key, value) {
    headers[key.toString()] = value.toString();
  });

  http.Response result = http.Response(
    decryptedResponse['body'] as String,
    decryptedResponse['statusCode'] as int,
    headers: headers,
  );
  return result;
}
