//
//  Converter.swift
//  AutoFillExtension
//

import Foundation
import Sodium

class ConverterService {
    
    /**
     Converts Bytes to Hex (represented as a string)
     
     - Parameter bytes: some bytes to convert to hex

     - Returns: The corresponding hex value (represented as a String)
     */
    public func toHex(bytes: Bytes?) -> String? {
        if bytes == nil {
            return nil
        }
        var hexString: String = ""
        for byte in bytes! {
            hexString.append(String(format:"%02X", byte))
        }
        return hexString
    }
    
    /**
    Converts a Hex (represented as a String) to Bytes
     
     - Parameter string: a string representation of hex values

     - Returns: The corresponding bytes
    */
    public func fromHex(string: String?) -> Bytes? {
        if string == nil {
            return nil
        }
        let length = string!.count
        if length & 1 != 0 {
            return nil
        }
        var bytes = Bytes()
        bytes.reserveCapacity(length/2)
        var index = string!.startIndex
        for _ in 0..<length/2 {
            let nextIndex = string!.index(index, offsetBy: 2)
            if let b = UInt8(string![index..<nextIndex], radix: 16) {
                bytes.append(b)
            } else {
                return nil
            }
            index = nextIndex
        }
        return bytes
    }
    
}

let converter = ConverterService()
