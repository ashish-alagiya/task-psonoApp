import 'dart:async';

import 'package:flutter/material.dart';
import 'package:psono/app_config.dart';
import 'package:psono/main_core.dart';
import 'package:psono/services/offline_cache.dart' as offlineCache;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await offlineCache.init();

  Timer(Duration(milliseconds: 10000), () {
    offlineCache.incrementalUpdate(ratelimit: true);
  });

  var configuredApp = new AppConfig(
    appName: 'Psono Dev',
    flavorName: 'development',
    defaultServerUrl:
        'https://b024-2003-d5-5f34-fb00-5429-41b7-6657-7140.ngrok-free.app/server',
    child: new MyApp(),
  );
  runApp(configuredApp);
}
