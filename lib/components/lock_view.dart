import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pinput/pinput.dart';
import 'package:psono/components/_index.dart' as component;

typedef bool PassCodeVerify(String? passcode);

class LockView extends StatefulWidget {
  final VoidCallback onSuccess;
  final VoidCallback? fingerFunction;
  final VoidCallback? signOut;
  final bool? fingerVerify;
  final int passLength;
  final PassCodeVerify passCodeVerify;

  LockView({
    required this.onSuccess,
    required this.passLength,
    required this.passCodeVerify,
    this.fingerFunction,
    this.signOut,
    this.fingerVerify = false,
  }) : assert(passLength <= 8);

  @override
  _LockViewState createState() => _LockViewState();
}

class _LockViewState extends State<LockView> {
  final controller = TextEditingController();
  final focusNode = FocusNode();

  @override
  void dispose() {
    controller.dispose();
    focusNode.dispose();
    super.dispose();
  }

  _fingerPrint() {
    if (widget.fingerVerify!) {
      widget.onSuccess();
    }
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      widget.fingerFunction!();
    });
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 200), () {
      _fingerPrint();
    });

    return component.ScaffoldDark(
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            Hero(
              tag: 'hero',
              child: SvgPicture.asset(
                'assets/images/logo.svg',
                semanticsLabel: 'Psono Logo',
                height: 70.0,
              ),
            ),
            const SizedBox(height: 24.0),
            Center(
              child: Text(
                FlutterI18n.translate(context, "ENTER_YOUR_PIN"),
                style: const TextStyle(
                  fontSize: 16,
                  color: Color(0xFF666666),
                ),
              ),
            ),
            const SizedBox(height: 24.0),
            Pinput(
              length: widget.passLength,
              hapticFeedbackType: HapticFeedbackType.lightImpact,
              cursor: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 9),
                    width: 22,
                    height: 2,
                    color: Colors.white,
                  ),
                ],
              ),
              controller: controller,
              autofocus: true,
              obscureText: true,
              obscuringCharacter: '*',
              focusNode: focusNode,
              defaultPinTheme: PinTheme(
                width: 56,
                height: 56,
                textStyle: const TextStyle(
                  fontSize: 36,
                  color: Colors.white,
                ),
                decoration: BoxDecoration(
                  color: Color(0xFF182535),
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Colors.transparent),
                ),
              ),
              validator: (code) {
                if (!widget.passCodeVerify(code)) {
                  controller.text = '';
                  return FlutterI18n.translate(context, "PIN_INCORRECT");
                }
                return null;
              },
            ),
            const SizedBox(height: 24.0),
            Center(
              child: Text(
                FlutterI18n.translate(context, "FORGOT_YOUR_PIN"),
                style: const TextStyle(
                  fontSize: 16,
                  color: Color(0xFF666666),
                ),
              ),
            ),
            Center(
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: FlutterI18n.translate(context, "LOGOUT"),
                      style: const TextStyle(
                        color: Color(0xFF666666),
                        decoration: TextDecoration.underline,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () async {
                          widget.signOut!();
                        },
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 48.0),
            GestureDetector(
              onTap: () {
                widget.fingerFunction!();
              },
              child: Image.asset(
                "assets/images/fingerprint.png",
                height: 40,
                width: 40,
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }
}
