import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/store.dart';
import 'package:psono/screens/custom_drawer.dart';
import 'package:psono/screens/pin_configuration/index.dart';
import 'package:psono/services/autofill.dart' as autofillService;

import './autofill_android.dart';
import './autofill_ios.dart';
import './datastore.dart';
import './entry_types.dart';
import './offline_cache.dart';
import './password_generator.dart';

class SettingsScreen extends StatefulWidget {
  static String tag = 'settings-screen';

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool? _autofillSupported = false;

  Future<void> initStateAsync() async {
    bool? autofillSupported = await autofillService.isSupported();

    setState(() {
      _autofillSupported = autofillSupported;
    });
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      initStateAsync();
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> settings = [
      Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'PASSWORD_GENERATOR')),
          leading: Icon(component.FontAwesome.calculator),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingPasswordGenerator(),
              ),
            );
          },
        ),
      ),
      Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'ENTRY_TYPES')),
          leading: Icon(component.FontAwesome.check_circle_o),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingEntryTypes(),
              ),
            );
          },
        ),
      ),
      Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'CHANGE_PIN')),
          leading: Icon(component.FontAwesome.unlock_alt),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => PinConfigurationScreen(),
              ),
            );
          },
        ),
      ),
      Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'DATASTORES')),
          leading: Icon(component.FontAwesome.database),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingDatastoreScreen(),
              ),
            );
          },
        ),
      ),
    ];

    if (!reduxStore.state.complianceDisableOfflineMode) {
      settings.add(Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'OFFLINE_CACHE')),
          leading: Icon(component.FontAwesome.plane),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingOfflineCacheScreen(),
              ),
            );
          },
        ),
      ));
    }

    if (_autofillSupported! && Platform.isAndroid) {
      settings.add(Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'AUTOFILL_SERVICE')),
          leading: Icon(component.FontAwesome.cogs),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingAutofillAndroidScreen(),
              ),
            );
          },
        ),
      ));
    }

    if (_autofillSupported! && Platform.isIOS) {
      settings.add(Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'AUTOFILL_SERVICE')),
          leading: Icon(component.FontAwesome.cogs),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingAutofillIOSScreen(),
              ),
            );
          },
        ),
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(context, 'SETTINGS')),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Color(0xFFebeeef),
      body: Container(
          padding: const EdgeInsets.all(5.0),
          child: ListView(
            children: settings,
          )),
      drawer: CustomDrawer(),
    );
  }
}
