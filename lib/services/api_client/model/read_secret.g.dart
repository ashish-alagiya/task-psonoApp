// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_secret.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadSecret _$ReadSecretFromJson(Map<String, dynamic> json) => ReadSecret(
      createDate: json['create_date'] as String?,
      writeDate: json['write_date'] as String?,
      data: fromHex(json['data'] as String?),
      dataNonce: fromHex(json['data_nonce'] as String?),
      type: json['type'] as String?,
      callbackUrl: json['callback_url'] as String?,
      callbackUser: json['callback_user'] as String?,
      callbackPass: json['callback_pass'] as String?,
    );

Map<String, dynamic> _$ReadSecretToJson(ReadSecret instance) =>
    <String, dynamic>{
      'create_date': instance.createDate,
      'write_date': instance.writeDate,
      'data': toHex(instance.data),
      'data_nonce': toHex(instance.dataNonce),
      'type': instance.type,
      'callback_url': instance.callbackUrl,
      'callback_user': instance.callbackUser,
      'callback_pass': instance.callbackPass,
    };
