package com.psono.psono.autofill

import androidx.annotation.RequiresApi
import android.app.PendingIntent
import android.content.Intent
import android.content.IntentSender
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import android.service.autofill.AutofillService
import android.service.autofill.FillCallback
import android.service.autofill.FillRequest
import android.service.autofill.FillResponse
import android.service.autofill.SaveCallback
import android.service.autofill.SaveRequest
import android.widget.RemoteViews
import com.psono.psono.R
import com.psono.psono.MainActivity

@RequiresApi(Build.VERSION_CODES.O)
class PsonoAutofillService: AutofillService() {
    /**
     * Called by the Android system do decide if a screen can be autofilled by the service.
     *
     * Inspired / copied from (under Mozilla Public License 2.0) :
     * https://github.com/mozilla-lockwise/lockwise-android/blob/df21ab6442f4a6315d0e3683139825646509a0aa/app/src/main/java/mozilla/lockbox/LockboxAutofillService.kt
     * https://github.com/mozilla-lockwise/lockwise-android/blob/afbd758bb9bfbfcf4910fbebc9bec28c46865530/app/src/main/java/mozilla/lockbox/autofill/ParsedStructureBuilder.kt
     *
     */
    override fun onFillRequest(request: FillRequest, cancellationSignal: CancellationSignal,
                               callback: FillCallback) {

        val structure = request.fillContexts.last().structure
        val activityPackageName = structure.activityComponent.packageName
        if (this.packageName == activityPackageName) {
            callback.onFailure(null)
            return
        }

        val nodeNavigator = ViewNodeNavigator(structure, activityPackageName)
        val parsedStructure = ParsedStructureBuilder(nodeNavigator).build() as ParsedStructure

        if (parsedStructure.passwordId == null && parsedStructure.usernameId == null) {
            callback.onFailure(null)
            return
        }

        val authIntent = Intent(this, MainActivity::class.java).apply {
            // Send any additional data required to complete the request.
            action = Intent.ACTION_SEND
            type = "text/plain"

            // pass the package name and web domain to the search funtion
            putExtra(Intent.EXTRA_TEXT, "autofill::" + parsedStructure.packageName + "::" + (parsedStructure.webDomain ?: ""))

            // open the route /autofill/ in the app per default
            putExtra("route", "/autofill/")

            // add the parsedStructure to the intent so can later use it to fill out the request
            val extras = Bundle()
            extras.putParcelable("parsedStructure", parsedStructure)
            putExtra("parsedStructure", extras)
        }


        val intentSender: IntentSender = PendingIntent.getActivity(
                this,
                1001,
                authIntent,
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) (PendingIntent.FLAG_IMMUTABLE or  PendingIntent.FLAG_CANCEL_CURRENT) else PendingIntent.FLAG_CANCEL_CURRENT
        ).intentSender

        val presentation = RemoteViews(this.packageName, R.layout.autofill_cta_presentation)
        val searchText = this.getString(R.string.autofill_search_cta, "Psono")
        presentation.setTextViewText(R.id.autofill_cta, searchText)

        val fillResponse: FillResponse = FillResponse.Builder()
                .setAuthentication(parsedStructure.autofillIds, intentSender, presentation)
                .build()

        // If there are no errors, call onSuccess() and pass the response
        callback.onSuccess(fillResponse)
    }

    /**
     * Called when the user requests the service to save the contents of a screen.
     */
    override fun onSaveRequest(request: SaveRequest, callback: SaveCallback) {
        callback.onFailure("onSaveRequest not implemented")
    }

    /**
     * Called when the Android system connects to service.
     */
    override fun onConnected() {
        super.onConnected()
    }

    /**
     * Called when the Android system disconnects from the service.
     */
    override fun onDisconnected() {
        super.onDisconnected()
    }


}