import 'dart:typed_data';
import 'package:psono/services/api_client/index.dart' as apiClient;

class CheckHostResult {
  CheckHostResult({this.serverUrl, this.status, this.verifyKey, this.info});

  final String? serverUrl;
  final String? status;
  final Uint8List? verifyKey;
  final apiClient.Info? info;
}
