import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/manager_datastore.dart' as managerDatastore;

class SettingDatastoreScreen extends StatefulWidget {
  static String tag = 'settings-datastore-screen';

  @override
  _SettingDatastoreScreenState createState() => _SettingDatastoreScreenState();
}

class _SettingDatastoreScreenState extends State<SettingDatastoreScreen> {
  List<apiClient.ReadDatastoreListEntry> _datastores = [];

  Future<void> loadDatastoreList() async {
    apiClient.ReadDatastoreList? readDatastoreList =
        await managerDatastore.getDatastoreOverview();

    setState(() {
      _datastores = readDatastoreList!.datastores!
          .where((e) => e.type == 'password')
          .toList();
    });
  }

  @override
  void initState() {
    super.initState();
    loadDatastoreList();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'DATASTORES',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Color(0xFFebeeef),
      body: Card(
        child: Container(
          constraints: BoxConstraints.expand(),
          child: DataTable(
            onSelectAll: (b) {},
            sortAscending: true,
            columns: <DataColumn>[
              DataColumn(
                label: Text(FlutterI18n.translate(
                  context,
                  'DESCRIPTION',
                )),
              ),
              DataColumn(
                label: Text(FlutterI18n.translate(
                  context,
                  'DEFAULT',
                )),
              ),
            ],
            rows: _datastores.map(
              (datastore) {
                var onChanged;
                if (!datastore.isDefault!) {
                  onChanged = (value) async {
                    await managerDatastore.saveDatastoreMeta(
                        datastore.id, datastore.description, true);
                    loadDatastoreList();
                  };
                }

                return DataRow(
                  cells: [
                    DataCell(
                      Text(datastore.description!),
                      showEditIcon: false,
                      placeholder: false,
                    ),
                    DataCell(Switch(
                      value: datastore.isDefault!,
                      onChanged: onChanged,
                    )),
                  ],
                );
              },
            ).toList(),
          ),
        ),
      ),
    );
  }
}
