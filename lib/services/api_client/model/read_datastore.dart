import 'package:json_annotation/json_annotation.dart';
import 'dart:typed_data';
import 'package:psono/services/converter.dart';

part 'read_datastore.g.dart';

@JsonSerializable()
class ReadDatastore {
  ReadDatastore({
    this.data,
    this.dataNonce,
    this.type,
    this.description,
    this.secretKey,
    this.secretKeyNonce,
    this.isDefault,
  });

  @JsonKey(fromJson: fromHex, toJson: toHex)
  final Uint8List? data;
  @JsonKey(name: 'data_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List? dataNonce;
  final String? type;
  final String? description;
  @JsonKey(name: 'secret_key', fromJson: fromHex, toJson: toHex)
  final Uint8List? secretKey;
  @JsonKey(name: 'secret_key_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List? secretKeyNonce;
  @JsonKey(name: 'is_default')
  final bool? isDefault;

  factory ReadDatastore.fromJson(Map<String, dynamic> json) =>
      _$ReadDatastoreFromJson(json);

  Map<String, dynamic> toJson() => _$ReadDatastoreToJson(this);
}
