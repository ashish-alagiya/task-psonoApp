import 'package:json_annotation/json_annotation.dart';

part 'delete_secret_link.g.dart';

@JsonSerializable()
class DeleteSecretLink {
  DeleteSecretLink();

  factory DeleteSecretLink.fromJson(Map<String, dynamic> json) =>
      _$DeleteSecretLinkFromJson(json);

  Map<String, dynamic> toJson() => _$DeleteSecretLinkToJson(this);
}
