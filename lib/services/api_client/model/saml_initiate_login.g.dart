// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'saml_initiate_login.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SamlInitiateLogin _$SamlInitiateLoginFromJson(Map<String, dynamic> json) =>
    SamlInitiateLogin(
      samlRedirectUrl: json['saml_redirect_url'] as String?,
    );

Map<String, dynamic> _$SamlInitiateLoginToJson(SamlInitiateLogin instance) =>
    <String, dynamic>{
      'saml_redirect_url': instance.samlRedirectUrl,
    };
