import 'package:flutter/material.dart';

class Btn extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;
  final Color? color;
  final Color textColor;
  final BorderRadiusGeometry borderRadius;
  Btn(
      {this.text,
      this.onPressed,
      this.borderRadius = BorderRadius.zero,
      this.color = const Color(0xFFE0E0E0),
      this.textColor = const Color(0xFF333333)});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 10,
      ),
      child: ElevatedButton(
        onPressed: () {
          onPressed!();
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          padding: const EdgeInsets.all(12),
          shape: RoundedRectangleBorder(
            borderRadius: borderRadius,
          ),
        ),
        child: Text(
          text!,
          style: TextStyle(color: textColor),
        ),
      ),
    );
  }
}
