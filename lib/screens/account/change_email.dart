import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/store.dart';
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;

class AccountChangeEmailScreen extends StatefulWidget {
  static String tag = 'account-change-email-screen';

  @override
  _AccountChangeEmailScreenState createState() =>
      _AccountChangeEmailScreenState();
}

class _AccountChangeEmailScreenState extends State<AccountChangeEmailScreen> {
  final email = TextEditingController(
    text: reduxStore.state.userEmail,
  );
  final password = TextEditingController(
    text: '',
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    component.Loader.hide();
    email.dispose();
    password.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    void _showErrorDiaglog(String title, String? content) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(FlutterI18n.translate(context, title)),
            content: Text(FlutterI18n.translate(context, content!)),
            actions: <Widget>[
              TextButton(
                child: Text(FlutterI18n.translate(context, "CLOSE")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'CHANGE_E_MAIL',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: const Color(0xFFebeeef),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: component.BtnSuccess(
            onPressed: () async {
              if (!_formKey.currentState!.validate()) {
                return;
              }
              component.Loader.show(context);
              try {
                await managerDatastoreUser.saveNewEmail(
                  email.text,
                  password.text,
                );
              } catch (e) {
                _showErrorDiaglog('ERROR', e.toString());
                return;
              } finally {
                component.Loader.hide();
              }

              password.text = '';

              if (!mounted) {
                return;
              }
              final SnackBar snackBar = SnackBar(
                content: Text(
                  FlutterI18n.translate(context, "SAVE_SUCCESS"),
                ),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
            text: FlutterI18n.translate(context, "SAVE"),
          ),
        ),
      ),
      body: Card(
        child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                component.AlertInfo(
                  text: FlutterI18n.translate(
                      context, "CHANGE_E_MAIL_DESCRIPTION"),
                ),
                TextFormField(
                  controller: email,
                  keyboardType: TextInputType.emailAddress,
                  autofocus: true,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return FlutterI18n.translate(
                        context,
                        'INVALID_EMAIL_IN_EMAIL',
                      );
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'NEW_E_MAIL',
                    ),
                  ),
                ),
                TextFormField(
                  obscureText: true,
                  controller: password,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return FlutterI18n.translate(
                        context,
                        'OLD_PASSWORD_REQUIRED',
                      );
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'CURRENT_PASSWORD',
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
