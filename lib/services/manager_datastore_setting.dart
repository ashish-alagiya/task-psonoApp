import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/manager_datastore.dart' as managerDatastore;

/// Returns the setting datastore.
Future<datastoreModel.Datastore> getSettingsDatastore() async {
  final String type = 'settings';

  datastoreModel.Datastore datastore = await managerDatastore.getDatastore(
    type,
  );

  bool passwordLengthFound = false;
  bool lettersUppercaseFound = false;
  bool lettersLowercaseFound = false;
  bool numbersFound = false;
  bool specialCharsFound = false;
  String passwordLength = "";
  String? lettersUppercase = "";
  String? lettersLowercase = "";
  String? numbers = "";
  String? specialChars = "";
  for (var i = 0; i < datastore.dataKV.length; i++) {
    if (datastore.dataKV[i]['key'] == 'setting_password_length') {
      passwordLength = datastore.dataKV[i]['value'].toString();
      passwordLengthFound = true;
    }
    if (datastore.dataKV[i]['key'] == 'setting_password_letters_uppercase') {
      lettersUppercase = datastore.dataKV[i]['value'];
      lettersUppercaseFound = true;
    }
    if (datastore.dataKV[i]['key'] == 'setting_password_letters_lowercase') {
      lettersLowercase = datastore.dataKV[i]['value'];
      lettersLowercaseFound = true;
    }
    if (datastore.dataKV[i]['key'] == 'setting_password_numbers') {
      numbers = datastore.dataKV[i]['value'];
      numbersFound = true;
    }
    if (datastore.dataKV[i]['key'] == 'setting_password_special_chars') {
      specialChars = datastore.dataKV[i]['value'];
      specialCharsFound = true;
    }
  }

  if (!passwordLengthFound) {
    datastore.dataKV.add({
      'key': 'setting_password_length',
      'value': '16',
    });
    passwordLength = '16';
  }
  if (!lettersUppercaseFound) {
    datastore.dataKV.add({
      'key': 'setting_password_letters_uppercase',
      'value': 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    });
    lettersUppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }
  if (!lettersLowercaseFound) {
    datastore.dataKV.add({
      'key': 'setting_password_letters_lowercase',
      'value': 'abcdefghijklmnopqrstuvwxyz',
    });
    lettersLowercase = 'abcdefghijklmnopqrstuvwxyz';
  }
  if (!numbersFound) {
    datastore.dataKV.add({
      'key': 'setting_password_numbers',
      'value': '0123456789',
    });
    numbers = '0123456789';
  }
  if (!specialCharsFound) {
    datastore.dataKV.add({
      'key': 'setting_password_special_chars',
      'value': ',.-;:_#\'+*~!"§\$%&/()=?{[]}\\',
    });
    specialChars = ',.-;:_#\'+*~!"§\$%&/()=?{[]}\\';
  }

  reduxStore.dispatch(
    PasswordGeneratorSettingAction(
      passwordLength,
      lettersUppercase,
      lettersLowercase,
      numbers,
      specialChars,
    ),
  );

  return datastore;
}
