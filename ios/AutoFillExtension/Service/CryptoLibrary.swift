//
//  CryptoLibrary.swift
//  AutoFillExtension
//

import Foundation
import Sodium

class CryptoLibraryService {
    
    
    private var sodium:Sodium!
    
    public init() {
        self.sodium = Sodium()
    }
    
    /**
     Takes some data and a secret key and encrypts the data with it.
     Returns the nonce and the cipher text
     
     - Parameter data; The data you want to encrypt
     - Parameter secretKey: The secret key you want to use to encrypt the data
     
     - Returns: The encrypted text and nonce
     */
    public func encryptData(data: String, secretKey: Bytes) -> (Bytes, Bytes) {
        let encrypteDataWithNonce: Bytes = self.sodium.secretBox.seal(message: Bytes(data.utf8), secretKey: secretKey)!
        let text = Array(encrypteDataWithNonce.suffix(encrypteDataWithNonce.count - 24))
        let nonce = Array(encrypteDataWithNonce.prefix(24))
        return (text, nonce)
    }
    
    /**
     Decrypts an encrypted text with a nonce and a secret key
     
     - Parameter text: the encrypted text as bytes
     - Parameter nonce: the nonce of the encrypted text
     - Parameter secretKey: the secret key used in the past to encrypt the text

     - Returns: The decrypted data
     */
    public func decryptData(text: Bytes, nonce: Bytes, secretKey: Bytes) -> String? {
        
        let dataBytes:Bytes? = self.sodium.secretBox.open(authenticatedCipherText: text, secretKey: secretKey, nonce: nonce)
        
        if dataBytes == nil {
            return nil
        }
        
        return String(bytes: dataBytes!, encoding: String.Encoding.utf8)
    }
    
    
}

let cryptoLibrary = CryptoLibraryService()
