import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:otp/otp.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:psono/services/helper.dart' as helper;

import 'icons.dart';

class TotpCode extends StatefulWidget {
  final String? code;
  final String? algorithm;
  final int? digits;
  final int? period;

  TotpCode({
    this.code,
    this.algorithm,
    this.digits,
    this.period,
  });

  @override
  _TotpCodeState createState() => _TotpCodeState();
}

class _TotpCodeState extends State<TotpCode> {
  int millisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch;
  late int countdown;
  int? length = 6; // default length 6
  Algorithm algorithm = Algorithm.SHA1; // default algorithm SHA1
  int? interval = 30; // default interval 30
  late double percent;
  late Timer timer;
  String generatedCode = '';
  late FToast _fToast;

  @override
  void initState() {
    super.initState();

    if (this.widget.digits != null) {
      length = this.widget.digits;
    }

    if (this.widget.algorithm == 'SHA256') {
      algorithm = Algorithm.SHA256;
    } else if (this.widget.algorithm == 'SHA512') {
      algorithm = Algorithm.SHA512;
    }

    if (this.widget.period != null) {
      interval = this.widget.period;
    }

    countdown = interval! -
        ((DateTime.now().millisecondsSinceEpoch ~/ 1000).round() % interval!);
    percent = 1 -
        (interval! -
                ((DateTime.now().millisecondsSinceEpoch ~/ 1000).round() %
                    interval!)) /
            interval!;

    _fToast = FToast();
    _fToast.init(context);

    timer = Timer.periodic(Duration(milliseconds: 500), (timer) {
      setState(() {
        if (helper.isValidTOTPCode(this.widget.code)) {
          millisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch;
          countdown = interval! -
              ((millisecondsSinceEpoch ~/ 1000).round() % interval!);
          percent = 1 - countdown / interval!;

          generatedCode = OTP.generateTOTPCodeString(
            this.widget.code!,
            millisecondsSinceEpoch,
            algorithm: algorithm,
            interval: interval!,
            length: length!,
            isGoogle: true,
          );
        } else {
          generatedCode = FlutterI18n.translate(context, 'INVALID_CODE');
        }
      });
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularPercentIndicator(
        percent: percent,
        animation: true,
        lineWidth: 16.0,
        circularStrokeCap: CircularStrokeCap.round,
        animateFromLastPercent: true,
        radius: 150.0,
        progressColor: Color(0xFF2dbb93),
        center: new TextButton(
          child: new RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: generatedCode,
                ),
                TextSpan(
                  text: " ",
                ),
                WidgetSpan(
                  child: Icon(
                    FontAwesome.clipboard,
                    size: 32,
                    color: Colors.black54,
                  ),
                ),
              ],
              style: TextStyle(
                color: Colors.black54,
                fontSize: 32.0,
              ),
            ),
          ),
          onPressed: () {
            Clipboard.setData(
              new ClipboardData(
                text: generatedCode,
              ),
            );
            _clipboardCopyToast();
          },
        ),
      ),
    );
  }

  _clipboardCopyToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Color(0xFF2dbb93),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            FontAwesome.clipboard,
            color: Colors.white,
          ),
          const SizedBox(
            width: 12.0,
          ),
          Text(
            FlutterI18n.translate(context, "CLIPBOARD_COPY"),
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );

    _fToast.showToast(
        child: toast,
        toastDuration: Duration(seconds: 2),
        positionedToastBuilder: (context, child) {
          return Positioned(child: child, top: 110, left: 0, right: 0);
        });
  }
}
