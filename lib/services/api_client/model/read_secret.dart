import 'package:json_annotation/json_annotation.dart';
import 'dart:typed_data';
import 'package:psono/services/converter.dart';

part 'read_secret.g.dart';

@JsonSerializable()
class ReadSecret {
  ReadSecret({
    this.createDate,
    this.writeDate,
    this.data,
    this.dataNonce,
    this.type,
    this.callbackUrl,
    this.callbackUser,
    this.callbackPass,
  });

  @JsonKey(name: 'create_date')
  final String? createDate;
  @JsonKey(name: 'write_date')
  final String? writeDate;
  @JsonKey(fromJson: fromHex, toJson: toHex)
  final Uint8List? data;
  @JsonKey(name: 'data_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List? dataNonce;
  final String? type;
  @JsonKey(name: 'callback_url')
  final String? callbackUrl;
  @JsonKey(name: 'callback_user')
  final String? callbackUser;
  @JsonKey(name: 'callback_pass')
  final String? callbackPass;

  factory ReadSecret.fromJson(Map<String, dynamic> json) =>
      _$ReadSecretFromJson(json);

  Map<String, dynamic> toJson() => _$ReadSecretToJson(this);
}
