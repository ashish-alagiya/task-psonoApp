import 'dart:typed_data';

import 'package:psono/model/config.dart';

class InitiateStateAction {
  final String serverUrl;
  final String? username;
  final String? token;
  final Uint8List? sessionSecretKey;
  final int? lastServerConnectionTimeSinceEpoch;
  final int? lastCacheTimeSinceEpoch;
  final Uint8List? secretKey;
  final Uint8List? publicKey;
  final Uint8List? privateKey;
  final String? lockscreenPin;
  final bool complianceDisableDeleteAccount;
  final bool complianceDisableOfflineMode;
  final int complianceMaxOfflineCacheTimeValid;
  final String? userId;
  final String? userEmail;
  final String? userSauce;
  final Config? config;
  InitiateStateAction(
    this.serverUrl,
    this.username,
    this.token,
    this.sessionSecretKey,
    this.lastServerConnectionTimeSinceEpoch,
    this.lastCacheTimeSinceEpoch,
    this.secretKey,
    this.publicKey,
    this.privateKey,
    this.lockscreenPin,
    this.complianceDisableDeleteAccount,
    this.complianceDisableOfflineMode,
    this.complianceMaxOfflineCacheTimeValid,
    this.userId,
    this.userEmail,
    this.userSauce,
    this.config,
  );
}

class SetLastServerConnectionTimeSinceEpochReducerAction {
  final int lastServerConnectionTimeSinceEpoch;
  SetLastServerConnectionTimeSinceEpochReducerAction(
    this.lastServerConnectionTimeSinceEpoch,
  );
}

class SetLastCacheTimeSinceEpochReducerAction {
  final int lastCacheTimeSinceEpoch;
  SetLastCacheTimeSinceEpochReducerAction(
    this.lastCacheTimeSinceEpoch,
  );
}

class SetUserEmailAction {
  final String userEmail;
  SetUserEmailAction(
    this.userEmail,
  );
}

class SetUserUsernameAction {
  final String? username;
  SetUserUsernameAction(
    this.username,
  );
}

class InitiateLoginAction {
  final String? serverUrl;
  final String username;
  InitiateLoginAction(
    this.serverUrl,
    this.username,
  );
}

class ConfigUpdatedAction {
  final Config config;
  ConfigUpdatedAction(
    this.config,
  );
}

class SetVerifyKeyAction {
  final Uint8List? verifyKeyOld;
  final Uint8List? verifyKey;
  final bool? complianceDisableDeleteAccount;
  final bool? complianceDisableOfflineMode;
  final int? complianceMaxOfflineCacheTimeValid;
  SetVerifyKeyAction(
    this.verifyKeyOld,
    this.verifyKey,
    this.complianceDisableDeleteAccount,
    this.complianceDisableOfflineMode,
    this.complianceMaxOfflineCacheTimeValid,
  );
}

class SetServerPolicyAction {
  final bool? complianceDisableDeleteAccount;
  final bool? complianceDisableOfflineMode;
  final int? complianceMaxOfflineCacheTimeValid;
  SetServerPolicyAction(
    this.complianceDisableDeleteAccount,
    this.complianceDisableOfflineMode,
    this.complianceMaxOfflineCacheTimeValid,
  );
}

class SetSessionInfoAction {
  final String? userId;
  final String? userEmail;
  final String? token;
  final Uint8List? sessionSecretKey;
  final Uint8List? secretKey;
  final Uint8List? publicKey;
  final Uint8List? privateKey;
  final String? userSauce;
  SetSessionInfoAction(
    this.userId,
    this.userEmail,
    this.token,
    this.sessionSecretKey,
    this.secretKey,
    this.publicKey,
    this.privateKey,
    this.userSauce,
  );
}

class UpdateLockscreenSettingAction {
  final String lockscreenPin;
  UpdateLockscreenSettingAction(
    this.lockscreenPin,
  );
}

class PasswordGeneratorSettingAction {
  final String passwordLength;
  final String? lettersUppercase;
  final String? lettersLowercase;
  final String? numbers;
  final String? specialChars;
  PasswordGeneratorSettingAction(
    this.passwordLength,
    this.lettersUppercase,
    this.lettersLowercase,
    this.numbers,
    this.specialChars,
  );
}
