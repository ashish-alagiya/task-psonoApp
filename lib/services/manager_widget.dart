import 'package:flutter/widgets.dart';
import 'package:path/path.dart' as p;
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_file_link.dart' as managerFileLink;
import 'package:psono/services/manager_secret_link.dart' as managerSecretLink;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:psono/services/manager_share_link.dart' as managerShareLink;

IconData? itemIcon(datastoreModel.Item item) {
  final IconData defaultIconClass = component.FontAwesome.file_o;
  final Map<String, IconData> iconClassMap = {
    'txt': component.FontAwesome.file_text_o,
    'log': component.FontAwesome.file_text_o,
    'jpg': component.FontAwesome.file_image_o,
    'jpeg': component.FontAwesome.file_image_o,
    'png': component.FontAwesome.file_image_o,
    'gif': component.FontAwesome.file_image_o,
    'pdf': component.FontAwesome.file_pdf_o,
    'wav': component.FontAwesome.file_audio_o,
    'mp3': component.FontAwesome.file_audio_o,
    'wma': component.FontAwesome.file_audio_o,
    'avi': component.FontAwesome.file_video_o,
    'mov': component.FontAwesome.file_video_o,
    'mkv': component.FontAwesome.file_video_o,
    'flv': component.FontAwesome.file_video_o,
    'mp4': component.FontAwesome.file_video_o,
    'mpg': component.FontAwesome.file_video_o,
    'doc': component.FontAwesome.file_word_o,
    'dot': component.FontAwesome.file_word_o,
    'docx': component.FontAwesome.file_word_o,
    'docm': component.FontAwesome.file_word_o,
    'dotx': component.FontAwesome.file_word_o,
    'dotm': component.FontAwesome.file_word_o,
    'docb': component.FontAwesome.file_word_o,
    'xls': component.FontAwesome.file_excel_o,
    'xlt': component.FontAwesome.file_excel_o,
    'xlm': component.FontAwesome.file_excel_o,
    'xla': component.FontAwesome.file_excel_o,
    'xll': component.FontAwesome.file_excel_o,
    'xlw': component.FontAwesome.file_excel_o,
    'xlsx': component.FontAwesome.file_excel_o,
    'xlsm': component.FontAwesome.file_excel_o,
    'xlsb': component.FontAwesome.file_excel_o,
    'xltx': component.FontAwesome.file_excel_o,
    'xltm': component.FontAwesome.file_excel_o,
    'xlam': component.FontAwesome.file_excel_o,
    'csv': component.FontAwesome.file_excel_o,
    'ppt': component.FontAwesome.file_powerpoint_o,
    'pptx': component.FontAwesome.file_powerpoint_o,
    'zip': component.FontAwesome.file_archive_o,
    'tar': component.FontAwesome.file_archive_o,
    'gz': component.FontAwesome.file_archive_o,
    '7zip': component.FontAwesome.file_archive_o
  };

  if (item.type == 'bookmark') {
    return component.FontAwesome.bookmark_o;
  }

  if (item.type == 'note') {
    return component.FontAwesome.sticky_note_o;
  }

  if (item.type == 'application_password') {
    return component.FontAwesome.cube;
  }

  if (item.type == 'website_password') {
    return component.FontAwesome.key;
  }

  if (item.type == 'totp') {
    return component.FontAwesome.qrcode;
  }

  if (item.type == 'user') {
    return component.FontAwesome.user;
  }

  if (item.type == 'mail_gpg_own_key') {
    return component.FontAwesome.lock;
  }

  if (item.type == 'credit_card') {
    return component.FontAwesome.credit_card;
  }

  if (item.type == 'ssh_own_key') {
    return component.FontAwesome.lock;
  }

  String extension = p.extension(item.name!);

  if (extension.length > 0) {
    extension = extension.substring(1);
    if (iconClassMap.containsKey(extension)) {
      return iconClassMap[extension];
    }
  }

  return defaultIconClass;
}

/// Deletes an item (or folder) from a datastore
///Takes care that the link structure on the server is updated
Future deleteItem(
  List<String?> items,
  List<String?> folders,
  List<String?>? relativePath,
  datastoreModel.Folder? parentShareFolder,
  datastoreModel.Datastore? parentDatastore,
  String datastoreType,
) async {
  shareModel.Share? parentShare;
  datastoreModel.Folder? parentFolder;

  if (parentShareFolder == null) {
    parentDatastore = await managerDatastorePassword.getPasswordDatastore(
      parentDatastore!.datastoreId,
    );
    parentFolder = parentDatastore.data;
  } else {
    parentShare = await managerShare.readShare(
      parentShareFolder.shareId,
      parentShareFolder.shareSecretKey,
    );
    parentFolder = parentShare.folder;
  }

  List<managerDatastorePassword.ChildItem> secretLinks = [];
  List<managerDatastorePassword.ChildItem> fileLinks = [];
  List<managerDatastorePassword.ChildShare> childShares = [];

  // Lets go through all items and find all secret links, file links nad child shares
  for (var i = 0; i < items.length; i++) {
    List<String?> itemPath = List.from(relativePath!);
    itemPath.add(items[i]);

    List search = managerDatastorePassword.findInDatastore(
      itemPath,
      parentFolder,
    );

    // can be in theory a Folder or an Item yet we expect an item
    var element = search[0][search[1]];
    if (!(element is datastoreModel.Item)) {
      continue;
    }

    search[0].removeAt(search[1]);

    // lets populate our child shares that we need to handle, e.g a we deleted a folder that contains some shares
    List<managerDatastorePassword.ChildShare> newChildShares = [];
    if (element.shareId != null) {
      newChildShares.add(managerDatastorePassword.ChildShare(
        element.shareId,
        null,
        element,
        List.from(relativePath)..add(element.id),
      ));
    }

    secretLinks.addAll(managerDatastorePassword.getAllSecretLinks(element));
    fileLinks.addAll(managerDatastorePassword.getAllFileLinks(element));
    childShares.addAll(newChildShares);
  }

  // Lets go through all folders and find all secret links, file links nad child shares
  for (var i = 0; i < folders.length; i++) {
    List<String?> folderPath = List.from(relativePath!);
    folderPath.add(folders[i]);

    List search = managerDatastorePassword.findInDatastore(
      folderPath,
      parentFolder,
    );

    var element = search[0][search[
        1]]; // can be in theory a Folder or an Item yet we expect a folder
    if (element is! datastoreModel.Folder) {
      continue;
    }

    search[0].removeAt(search[1]);

    // lets populate our child shares that we need to handle, e.g a we deleted a folder that contains some shares
    List<managerDatastorePassword.ChildShare> newChildShares = [];
    if (element.shareId != null) {
      newChildShares.add(
        managerDatastorePassword.ChildShare(
          element.shareId,
          element,
          null,
          List.from(relativePath)..add(element.id),
        ),
      );
    } else {
      managerDatastorePassword.getAllChildSharesByPath(
        List.from(relativePath)..add(element.id),
        parentFolder,
        newChildShares,
        element,
      );
    }

    secretLinks.addAll(managerDatastorePassword.getAllSecretLinks(element));
    fileLinks.addAll(managerDatastorePassword.getAllFileLinks(element));
    childShares.addAll(newChildShares);
  }

  // lets update for every child_share the share_index
  for (var i = childShares.length - 1; i >= 0; i--) {
    managerDatastorePassword.onShareDeleted(
      childShares[i],
      parentDatastore,
      parentShare,
    );
  }

  // and save everything (before we update the links and might lose some necessary rights)
  if (parentDatastore != null) {
    await parentDatastore.save();
  }
  if (parentShare != null) {
    await parentShare.save();
  }

  // adjust the links for every child_share (and therefore update the rights)
  for (var i = childShares.length - 1; i >= 0; i--) {
    if (childShares[i].folder != null) {
      managerShareLink.onShareDeleted(childShares[i].folder!.id);
    } else {
      managerShareLink.onShareDeleted(childShares[i].item!.id);
    }
  }

  // adjust the links for every secret link (and therefore update the rights)
  for (var i = secretLinks.length - 1; i >= 0; i--) {
    managerSecretLink.onSecretDeleted(secretLinks[i].id);
  }

  // adjust the links for every secret link (and therefore update the rights)
  for (var i = fileLinks.length - 1; i >= 0; i--) {
    managerFileLink.onFileDeleted(fileLinks[i].id);
  }
}
