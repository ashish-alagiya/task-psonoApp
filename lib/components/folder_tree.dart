import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/screens/download_file/index.dart';
import 'package:psono/screens/edit_secret/index.dart';
import 'package:psono/screens/folder/index.dart';
import 'package:psono/services/autofill.dart' as autofillService;

import './folder.dart' as componentFolder;
import './item.dart' as componentItem;

class FolderTree extends StatelessWidget {
  final datastoreModel.Folder? root;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<String?>? path;
  final List<String?>? relativePath;
  final datastoreModel.FolderCallback? onLongPressFolder;
  final datastoreModel.ItemCallback? onLongPressItem;
  final List<datastoreModel.Folder?> filteredFolders = [];
  final List<datastoreModel.Item?> filteredItems = [];
  final bool? autofill;

  FolderTree({
    this.root,
    this.datastore,
    this.share,
    this.path,
    this.relativePath,
    this.onLongPressFolder,
    this.onLongPressItem,
    this.autofill,
  });

  @override
  Widget build(BuildContext context) {
    int _getFolderCount() {
      return filteredFolders.length;
    }

    int _getItemCount() {
      return filteredItems.length;
    }

    int _calculateEntryCount() {
      return _getFolderCount() + _getItemCount();
    }

    bool filterDeleted(datastoreEntry) {
      return datastoreEntry.deleted != true;
    }

    void filterContent(datastoreModel.Folder? root, List<String?>? path) {
      if (root == null) {
        return;
      }

      if (root.folders != null) {
        for (var i = 0; i < root.folders!.length; i++) {
          if (filterDeleted(root.folders![i])) {
            filteredFolders.add(root.folders![i]);
          }
        }
      }
      if (root.items != null) {
        for (var i = 0; i < root.items!.length; i++) {
          if (filterDeleted(root.items![i])) {
            filteredItems.add(root.items![i]);
          }
        }
      }
      filteredFolders.sort((a, b) {
        if (a!.name == null) {
          return -1;
        }
        if (b!.name == null) {
          return 1;
        }
        return a.name!.compareTo(b.name!);
      });
      filteredItems.sort((a, b) {
        if (a!.name == null) {
          return -1;
        }
        if (b!.name == null) {
          return 1;
        }
        return a.name!.compareTo(b.name!);
      });
    }

    filterContent(root, path);

    if (_calculateEntryCount() == 0) {
      return SliverFillRemaining(
        hasScrollBody: false,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 50.0,
            ),
            child: Text(
              FlutterI18n.translate(
                context,
                (path != null && path!.length > 0)
                    ? "YOU_HAVE_NO_ENTRIES_IN_YOUR_FOLDER"
                    : "YOU_HAVE_NO_ENTRIES_IN_YOUR_DATASTORE",
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      );
    }

    return SliverGrid(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 150.0,
        mainAxisSpacing: 0.0,
        crossAxisSpacing: 0.0,
        childAspectRatio: 1.0,
      ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          int folderCount = _getFolderCount();
          if (index < folderCount) {
            datastoreModel.Folder folder = filteredFolders[index]!;
            return componentFolder.Folder(
              folder: folder,
              onLongPress: (datastoreModel.Folder? folder) {
                if (onLongPressFolder != null) {
                  onLongPressFolder!(folder);
                }
              },
              onPressed: () async {
                datastoreModel.Folder? newShare = share;
                List<String?> newRelativePath;
                if (folder.shareId != null) {
                  newShare = folder;
                  newRelativePath = [];
                } else {
                  newRelativePath = List.from(relativePath!)
                    ..addAll([folder.id]);
                }

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FolderScreen(
                      autoNavigate: [],
                      folder: folder,
                      datastore: datastore,
                      share: newShare,
                      path: List.from(path!)..addAll([folder.id]),
                      relativePath: newRelativePath,
                      autofill: autofill,
                    ),
                  ),
                );
              },
              isShare: folder.shareId != null,
              color: Color(0xFF151f2b),
            );
          } else {
            datastoreModel.Item item = filteredItems[index - folderCount]!;
            return componentItem.Item(
              item: item,
              onLongPress: (datastoreModel.Item? item) {
                if (onLongPressItem != null) {
                  onLongPressItem!(item);
                }
              },
              onPressed: () async {
                if (autofill ?? false) {
                  await autofillService.autofill(item.secretId, item.secretKey);
                } else {
                  if (item.fileId != null) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DownloadFileScreen(
                          parent: root,
                          datastore: datastore,
                          share: share,
                          item: item,
                          path: path,
                          relativePath: relativePath,
                        ),
                      ),
                    );
                  } else {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => EditSecretScreen(
                          parent: root,
                          datastore: datastore,
                          share: share,
                          item: item,
                          path: path,
                          relativePath: relativePath,
                        ),
                      ),
                    );
                  }
                }
              },
              isShare: item.shareId != null,
              color: Color(0xFF151f2b),
            );
          }
        },
        childCount: _calculateEntryCount(),
      ),
    );
  }
}
