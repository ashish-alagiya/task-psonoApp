import 'dart:typed_data';

import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/redux/store.dart';
import 'package:psono/model/encrypted_data.dart';

/// encrypts some data with user's secret-key-crypto
///
/// @param data The data you want to encrypt
///
/// @returns The encrypted text and the nonce
Future<EncryptedData> encryptSecretKey(String data) async {
  return cryptoLibrary.encryptData(data, reduxStore.state.secretKey);
}

/// decrypts some data with user's secret-key-crypto
///
/// @param text The encrypted text
/// @param text nonce The nonce of the encrypted text
///
/// @returns The decrypted data
Future<String> decryptSecretKey(Uint8List text, Uint8List nonce) async {
  return cryptoLibrary.decryptData(text, nonce, reduxStore.state.secretKey);
}
